-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2017 at 10:57 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bizeto_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `psw` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `psw`) VALUES
(1, 'rahul', 'rahul@optroninfo.com', '123456'),
(2, 'amit', 'amit@optroninfo.com', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `ad_table`
--

CREATE TABLE `ad_table` (
  `ad_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `approve_status` varchar(50) NOT NULL,
  `cat_name` varchar(250) NOT NULL,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `ad_title` varchar(100) NOT NULL,
  `ad_desc` text NOT NULL,
  `ad_type` varchar(20) NOT NULL,
  `ad_image` varchar(50) NOT NULL,
  `ad_image1` varchar(100) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `contact_mobile` varchar(50) NOT NULL,
  `contact_email` varchar(50) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `approve_by` varchar(50) NOT NULL,
  `posted_by` varchar(50) NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ad_view` varchar(100) NOT NULL,
  `seo_title` varchar(100) NOT NULL,
  `seo_desc` varchar(250) NOT NULL,
  `seo_url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ad_table`
--

INSERT INTO `ad_table` (`ad_id`, `cat_id`, `approve_status`, `cat_name`, `user_id`, `admin_id`, `ad_title`, `ad_desc`, `ad_type`, `ad_image`, `ad_image1`, `contact_name`, `contact_mobile`, `contact_email`, `company_name`, `city`, `approve_by`, `posted_by`, `post_date`, `ad_view`, `seo_title`, `seo_desc`, `seo_url`) VALUES
(1, 1, '1', '', 0, 0, 'Dont trust on sugar', 'Dont trust on sugar desfription grg', 'Free', '16 - 1.jpg', '16 - 1.jpg', 'Sugar name', '9890219091', '1@brainpoint.com', 'Brainpoint Computer Centre', 'Grant Road', '', '', '2017-12-06 05:06:30', '', '', '', ''),
(3, 2, '1', '', 41, 0, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 'Laptop For sale very good in condition ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix.if you want then contact us on following number', 'Free', '', '', 'Amit Desai', '9874587256', 'oldlaptop@gmail.com', 'Dont know', 'Mumbai', '', '', '2017-12-06 05:29:09', '', '', '', ''),
(4, 2, '1', 'tour and travels', 41, 0, 'Doorstep Laptop Repair Mumbai| Computer Repair & Servicesâ€Ž', 'Service to your door Windows installation with all \r\n\r\nAnywhere in hyderabad Secunderabad\r\n\r\nWindows (xp,7,8,10) installation DOOR Steep-HYD-LAPTOP-PC Repair\r\n\r\nVirus Removed \r\n\r\nInstallation OS Computer LAPTOP .\r\n\r\nsoftwares installation are Ms offi', 'Free', 'Webp.net-compress-image.jpg', 'hp.jpg', 'Anil Tripathi', '8796548754', 'repairservice@gmail.com', 'Computer repair services', 'Mumbai', '', '', '2017-12-06 05:32:25', '', '', '', ''),
(5, 3, '1', 'tour and travels', 41, 0, 'Learn English faster using Rapidx Speaking Course', 'Learn English faster using Rapidx Speaking Course - For Native Speakers', 'Free', 'rapidex-english-speaking.png', '', 'Sameer Shah', '9800000000', 'englishtutor@gmail.com', 'Rapidx Speaking ', 'Mumbai', '', '', '2017-12-06 05:42:09', '', '', '', ''),
(6, 3, '1', '', 41, 0, 'English Speaking Course Made Easy', 'Spoken English Course Basic & Advanced\r\n\r\nBENEFITS\r\n\r\nLearn to speak with flow & rythmn.\r\n\r\nSpeak with authority & vocal presence.\r\n\r\nAccent Softening.\r\n\r\nSpeak with greater clarity & confidence.\r\n\r\nCourse Content\r\n\r\nPronunciatian.\r\nSpelling & Sound ', 'Free', '', '', 'James', '9867497628', 'coaching@gmail.com', 'English Speaking Course', 'Mumbai', '', '', '2017-12-06 05:54:10', '', '', '', ''),
(8, 4, '1', '', 41, 0, 'Working Condition Second hand Laptop Rs 5000 Dell', 'Used Good Working Condition Second hand Laptop Rs.5000 Dell for sale.500 Gb Hdd, Intel core 2 Processor', 'Premium', 'second.jpg', 'second-hand-laptop-rs5000-dell-mumbai.jpg', 'Mr. Sharma', '8898546875', 'info@gmail.com', 'usedlaptops12@gmail.com', 'Mumbai', '', '', '2017-12-06 06:09:15', '', '', '', ''),
(9, 4, '1', 'courses', 0, 0, 'PHP Developer Course in Mumbai', 'Learn to build Enterprise Web Applications and Dynamic Web Portals. Our easy to understand teaching methodology with simple examples boosts your confidence at work.\r\nOn completing this course you will be able to work as a Web Application consultant o', 'Free', 'php-course.jpg', 'php-courses-1-265.jpg', 'Mr. Mahesh', '9875462151', 'ostrainer@gmail.com', 'Os Trainer', 'Mumbai', '', '', '2017-12-06 06:15:31', '', '', '', ''),
(10, 4, '1', '', 0, 0, 'Data Recovery Company in Mumbai', 'We recover data from formatted and undetectable Drives. corrupted hard disk, pen drives etc. call us immediately on following number', 'Free', 'data-recovery.jpg', '7888622_orig.jpg', 'Mr. Prabhu', '9887546321', 'recoveryhdd@gmail.com', 'Star', 'Mumbai', '', '', '2017-12-06 06:21:40', '', '', '', ''),
(11, 2, '1', '', 0, 0, 'English (Grammar) Speaking Course, IELTS, TOEFL and IBPS coaching', 'Spoken English including reading, writing, grammar, correct pronunciation (general / business English / commercial letter writing included) and soft skills to motivate and develop confidence for vernacular medium students (graduates, post graduates, ', 'Free', 'learnEnglish.jpg', 'aptech-english-speaking.jpg', 'Bryan', '9870564587', 'speaking@gmail.com', 'English (Grammar) Speaking Course', 'Mumbai', '', '', '2017-12-06 06:29:38', '', '', '', ''),
(12, 3, '1', '', 0, 0, 'Laptop $ computer repairing Services', 'Laptop $ computer repairing Services in mumbai with 24 by 7. we are experts in laptop repair and deliver result as soon as possible.', 'Free', 'image-repir.jpg', 'laptop-repair.jpg', 'Amit Nishad', '9890548765', 'repairlaptops@gmail.com', 'Laptop $ computer services', 'Mumbai', '', '', '2017-12-06 06:39:39', '', '', '', ''),
(13, 4, '1', '', 0, 0, 'MacBook Pro 15', 'Apple MacBook Pro 15\" Laptop \r\n\r\nPrice - 36,000\r\n\r\nSpecification : \r\n\r\nIntel Core 2 Duo Processor \r\n4 GB RAM \r\n320 GB Hard Drive ', 'Free', 'mac-second-hand.jpg', 'Fairly-Used-Apple-Mac.jpg', 'Bharti', '9875462154', 'secondlaptops@gmail.com', 'None', 'Andheri', '', '', '2017-12-06 06:44:28', '', '', '', ''),
(14, 4, '1', 'services', 0, 0, 'Mobile Reparing Course', 'Mobile Re paring Course . Hardware & Software in Mumbai we are provide full mobile repairing and other accessories repairing training in our institute.', 'Free', 'download.jpg', 'mobicomguru-mobile-repairing.png', 'Mr. Sarthak', '8793548756', 'starinstitude@gmail.com', 'Star Institute', 'Ghatkopar, Mumbai', '', '', '2017-12-06 06:55:29', '', '', '', ''),
(15, 3, '0', '', 0, 0, 'java 2', 'java desc 2', 'Premium', 'images-java.jpg', 'java-training-thane-mumbai.jpg', 'java name', '3333333333', 'java@gmaill.com', 'Indian Institute of Hardware Technology', 'Andheri, Mumbai', '', '', '2017-12-06 07:09:58', '', '', '', ''),
(16, 2, '1', '', 0, 0, 'All Types Of Computer, Laptop, Notebook, MacBook, Printers repairing..', 'We are providing all types of \r\nDesktop, \r\nLaptop, \r\nMac Laptop & Desktop\r\nLCD Monitor, \r\nPrinter, \r\nGraphic card Installation and repairing, \r\nTonner Refilling, \r\nHome and Office Networking, \r\nWifi Router Configuration, \r\nUpgradation & Installation ', 'Free', 'computer-repair-services.jpg', '$_86.JPG', 'AK shaikh', '7039393303', 'comparesolution@gmail.com', ' PERFECT COMPCARE SOLUTIONS', 'Bandra, Mumbai', '', '', '2017-12-06 07:16:46', '', '', '', ''),
(17, 1, '1', '', 0, 0, 'Its an sell my laptop good conditions HP Compaq', 'Its an sell my laptop good conditions HP Compaq predation cq40 ', 'Free', 'laptoprepair12.jpg', 'aaa1.JPG', 'Shanawaz', '9875000000', 'shanawaz1993@gmail.com', 'None', 'Mazagoan, Mumbai', '', '', '2017-12-06 07:30:54', '', '', '', ''),
(18, 4, '1', '', 0, 0, 'Canon EOS 80D 24.2MP Digital SLR Camera - Black (Body Only).', 'Canon EOS 80D 24.2MP Digital SLR Camera - Black (Body Only). Interested buyers can drop whatsapp number, I will contact you directly', 'Free', '81lzoBFOJXL._SX438_.jpg', 'download-camera.jpg', 'Mr. Prabhat', '9879231548', 'prabhat123roy@gmail.com', 'None', 'Boriwali, Mumbai', '', '', '2017-12-06 07:46:49', '', '', '', ''),
(19, 3, '1', '', 0, 0, 'Redmi 3s Prime in excellent condition with box packing', 'Redmi 3s Prime 32 GB in excellent condition with box packing\r\n\r\nItem age more than 12 month with charger .', 'Free', 'Redmi-Note-3-Review-2.jpg', 'xiaomi-redmi-3s-prime-back.jpg', 'Jyoti Kadam', '8793548769', 'kadam12jyoti@gmail.com', 'none', 'Ghatkopar, Mumbai', '', '', '2017-12-06 07:52:41', '', '', '', ''),
(20, 2, '1', '', 0, 0, '', '', 'Free', '', 'speakmore.jpg', '', '9878452165', 'speak7days@gmail.com', 'Speak English', 'Matunga, Mumbai', '', '', '2017-12-06 07:58:03', '', '', '', ''),
(21, 1, '0', '', 0, 0, '', '', 'Free', '', '', '', '', '', '', '', '', '', '2017-12-06 08:06:19', '', '', '', ''),
(22, 0, '0', 'Advertising & Marketing', 0, 0, 'cxscsxc', 'cscsxc', 'Free', '', '', 'sccs', 'cssc', '', 'companyname', 'mumbai', '', '', '2017-12-23 06:16:43', '', '', '', ''),
(23, 0, '0', 'Advertising & Marketing', 0, 1, 'cxscsxc', 'cscsxc', 'Free', '', '', 'sccs', 'cssc', '', 'companyname', 'mumbai', '', '', '2017-12-23 06:21:07', '', '', '', ''),
(24, 0, '0', 'tour and travels', 0, 1, 'aaaaa', 'aaaaa', 'Free', '3.jpg', '3.jpg', 'name ', '9890219809', 'ewf@gmail.com', 'companyname', 'mumbai', '', '', '2017-12-23 06:21:34', '', '', '', ''),
(25, 0, '0', 'tour and travels', 0, 1, 'fvfv', 'fvfv', 'Free', '', '', 'dfv', '9987542154', 'dwq@mir.com', 'companyname', 'mumbai', '', '', '2017-12-23 06:23:29', '', '', '', ''),
(26, 0, '0', 'tour and travels', 0, 1, 'fvfv', 'fvfv', 'Free', '', '', 'dfv', '9987542154', 'dwq@mir.com', 'companyname', 'mumbai', '', '', '2017-12-23 06:28:34', '', '', '', ''),
(27, 0, '0', 'Advertising & Marketing', 0, 1, 'vdfvfd', 'vfvfvfv', 'Free', '3.jpg', '3.jpg', 'name', '9890219154', 'freu@jujfr.com', 'companyname', 'mumbai', '', '', '2017-12-23 06:29:07', '', '', '', ''),
(28, 0, '0', 'Advertising & Marketing', 0, 1, 'vdfvfd', 'vfvfvfv', 'Free', '3.jpg', '3.jpg', 'name', '9890219154', 'freu@jujfr.com', 'companyname', 'mumbai', '', '', '2017-12-23 06:32:17', '', '', '', ''),
(29, 0, '0', 'Advertising & Marketing', 0, 1, 'vdv', 'dvdfv', 'Free', '3.jpg', '91zc0CBnbLL._UY445_ (3).jpg', 'vdsvdv', '9890215487', 'dfewn@nvujrv.com', 'companyname', 'mumbai', '', '', '2017-12-23 06:35:31', '', '', '', ''),
(30, 0, '0', 'tour and travels', 0, 1, 'xcdc', ' dcd', 'Free', '3.jpg', '2.jpg', 'dvdvcdv', '9890015487', 'dsbh@uf.com', 'companyname', 'mumbai', '', '', '2017-12-23 08:41:23', '', 'xxsx', 'cxsxcs', 'xsxcsc'),
(31, 0, '0', 'tour and travels', 0, 1, 'xcdc', ' dcd', 'Free', '3.jpg', '2.jpg', 'dvdvcdv', '9890015487', 'dsbh@uf.com', 'companyname', 'mumbai', '', '', '2017-12-23 08:41:47', '', 'xxsxsxsx', 'cxsxcsxsx', 'xsxcscsxs'),
(32, 0, '0', 'tour and travels', 0, 1, 'cssc', 'dscdsc', 'Free', '91zc0CBnbLL._UY445_.jpg', '3.jpg', 'dcvdcvd', '9890219092', 'vdvdv@ren.com', 'companyname', 'mumbai', '', '', '2017-12-23 08:43:05', '', 'cfdefer', 'fwrefrf', 'rfvrfvr'),
(33, 0, '1', '', 0, 1, 'vdvd', 'fvfdvdf', 'Free', '', '', 'dvdv', '9890219091', 'dv@fer.com', 'companyname', 'mumbai', '', '', '2017-12-23 09:03:43', '', 'fdv', 'fvfvfv', 'vfv');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(250) NOT NULL,
  `seourl` varchar(100) NOT NULL,
  `seotitle` varchar(100) NOT NULL,
  `cat_image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`, `seourl`, `seotitle`, `cat_image`) VALUES
(1, 'tour and travels', '', '', ''),
(2, 'Advertising & Marketing', '', '', ''),
(3, 'name of category', 'seo-url', 'seo-title', ''),
(4, 'Electronics', 'electronics', 'electronics-category', '');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `comp_id` int(11) NOT NULL,
  `approve_status` int(11) NOT NULL,
  `comp_title` varchar(250) NOT NULL,
  `seo_title` varchar(250) NOT NULL,
  `seo_desc` text NOT NULL,
  `seo_url` varchar(250) NOT NULL,
  `comp_desc` text NOT NULL,
  `comp_address` text NOT NULL,
  `contact_person` varchar(250) NOT NULL,
  `contact_mobile` varchar(20) NOT NULL,
  `contact_email` varchar(50) NOT NULL,
  `comp_website` varchar(50) NOT NULL,
  `comp_logo` varchar(50) NOT NULL,
  `views` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comp_type` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`comp_id`, `approve_status`, `comp_title`, `seo_title`, `seo_desc`, `seo_url`, `comp_desc`, `comp_address`, `contact_person`, `contact_mobile`, `contact_email`, `comp_website`, `comp_logo`, `views`, `created_date`, `comp_type`, `user_id`, `admin_id`) VALUES
(1, 1, 'optron', '', '', 'optron', 'optron', 'goragonn', 'bhavesh gudhka', '9833189090', 'email@gmail.com', '', '', 0, '2017-12-20 10:08:46', '', 41, 0),
(15, 0, '', '', '', 'optron 1', '', '', '', '', '', '', '', 0, '2017-12-21 07:39:02', '', 41, 0),
(16, 0, '', '', '', 'optron-21', '', '', '', '', '', '', '', 0, '2017-12-21 07:41:44', '', 41, 0),
(17, 0, '', '', '', 'optron-55', '', '', '', '', '', '', '', 0, '2017-12-21 07:42:04', '', 41, 0),
(18, 0, 'vdvd', '', '', 'edv-dwqqsd', 'vdvdv', 'devfev', 'dvv', '9890219091', 'cdscv@vn.com', 'fedfve', '', 0, '2017-12-23 06:43:42', 'Free', 0, 0),
(19, 0, 'vdvdcsc', '', '', 'edv-dwqqsdsc', 'vdvdv', 'devfev', 'dvv', '9890219091', 'cdscv@vn.com', 'fedfve', '', 0, '2017-12-23 06:44:02', 'Free', 0, 0),
(20, 0, 'vdvdcsc', '', '', 'edv-dwqqsdscdfved', 'vdvdv', 'devfev', 'dvv', '9890219091', 'cdscv@vn.com', 'fedfve', '', 0, '2017-12-23 06:45:12', 'Free', 0, 1),
(21, 1, 'aaaaaaaaaaa', 'seo title', 'seo desc', 'aaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaa', 'fvrfvrvr', 'aaaaaaaaa', '9890219092', 'fcedn@jnf.com', 'aaaa', '3.jpg', 0, '2017-12-23 08:37:20', 'Premium', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `comp_categories`
--

CREATE TABLE `comp_categories` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `seo_url` varchar(100) NOT NULL,
  `seo_title` varchar(100) NOT NULL,
  `cat_image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comp_categories`
--

INSERT INTO `comp_categories` (`cat_id`, `cat_name`, `seo_url`, `seo_title`, `cat_image`) VALUES
(1, 'name of cATEGORY 1', 'seo-url', 'seo title', '');

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `e_id` int(11) NOT NULL,
  `adid` int(11) NOT NULL,
  `adtitle` varchar(250) NOT NULL,
  `cname` varchar(100) NOT NULL,
  `cnumber` varchar(50) NOT NULL,
  `cmessage` varchar(250) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`e_id`, `adid`, `adtitle`, `cname`, `cnumber`, `cmessage`, `user_id`) VALUES
(1, 0, '', 'cscs', 'cscscsc', 'cdscd', 0),
(2, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 'cdsc', 'dcdcd', 'cdcvdvcd', 41),
(3, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 'rahul patil', '9890219091', 'write your message here', 41),
(4, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 'dcv', 'dcdcdc', 'dcvdcvd', 41),
(5, 0, '', 'CDSCD', 'CDCD', 'CDCDCD', 0),
(6, 0, '', ' XC C', 'D CDVD', 'V DVDVD', 0),
(7, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 'CSCD', 'SCDSC', 'DCDCDC', 41),
(8, 0, '', 'CSXC', 'SCSCS', 'CSCDS', 0),
(9, 0, '', 'CDC', 'DCDCD', 'CDCDCD', 0),
(10, 0, '', 'scxc', 'dscdsc', 'dcdscdc', 0),
(11, 0, '', 'cscs', 'cscscsd', 'csdcdc', 0),
(12, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 'csc', 'csc', 'scscsdc', 41),
(13, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 's', 'dsdc', 'dcfedfedfe', 41),
(14, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 'cdscdc', 'scdcd', 'cdcdcdcd', 41),
(15, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 'csdcdcd', 'xdvdvdv', 'dccdcvd', 41),
(16, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 'sccscsc', 'sccsc', 'cscscsd', 41),
(17, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 'vdv', 'dvdfv', 'fdvfdvfv', 41),
(18, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', ' x', ' x x', 'x x', 41),
(19, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 'name of contact', '8978548754', 'I am Interested in ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price ', 41),
(20, 3, 'ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price', 'cscscdscd', 'cscsc', 'I am Interested in ACER 3rd GEN coer i5 4gb /320gb hdd Laptop rs.13000 fix price ', 41),
(21, 10, 'Data Recovery Company in Mumbai', 'sdede', '9890219095', 'I am Interested in Data Recovery Company in Mumbai ', 0);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `rev_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `rev_name` varchar(100) NOT NULL,
  `rev_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rev_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`rev_id`, `comp_id`, `rev_name`, `rev_date`, `rev_desc`) VALUES
(1, 1, 'rahul', '2017-12-20 10:49:50', 'esfcefewf'),
(2, 1, 'rahul', '2017-12-20 10:49:53', 'edcfedfew'),
(3, 1, 'rahul', '2017-12-20 10:49:59', 'rahul');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `subcat_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `subcategory_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`subcat_id`, `cat_id`, `subcategory_name`) VALUES
(1, 1, 'tours'),
(2, 3, 'nothing'),
(3, 4, 'Mobiles & Tablets'),
(4, 4, 'Printers'),
(5, 0, ''),
(6, 0, ''),
(7, 0, ''),
(8, 0, ''),
(9, 0, ''),
(10, 0, ''),
(11, 0, ''),
(12, 0, ''),
(13, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_mobile` varchar(100) NOT NULL,
  `user_pass` varchar(100) NOT NULL,
  `user_role` int(11) NOT NULL,
  `user_status` varchar(250) NOT NULL,
  `user_code` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_email`, `user_mobile`, `user_pass`, `user_role`, `user_status`, `user_code`, `created_at`) VALUES
(1, 'Amit Sawant', 'amitsawant1993@gmail.com', '8898586881', 'efe', 1, '', '', '2017-11-20 05:01:13'),
(41, 'rahul', 'rahul@gmail.com', '9890215487', 'rahul@123', 3, '0', 'VRkWTlyt04hobFmq1nDg', '2017-12-14 06:46:07'),
(42, 'shubham', 'shubham@patil.com', '9890219854', 'shubham', 3, '0', 'M3ShQEGNbJu5oWkranX8', '2017-12-14 06:47:03'),
(44, 'dwsde', 'reojfire@jvf.com', '9890219091', '9b56e724cd1be4ff42360f2d74e0e9d2d46d008b', 3, '0', 'I4EyLjoG3FzrkMvCV6AJ', '2017-12-14 10:34:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_table`
--
ALTER TABLE `ad_table`
  ADD PRIMARY KEY (`ad_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`comp_id`);

--
-- Indexes for table `comp_categories`
--
ALTER TABLE `comp_categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`rev_id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`subcat_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ad_table`
--
ALTER TABLE `ad_table`
  MODIFY `ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `comp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `comp_categories`
--
ALTER TABLE `comp_categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `e_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `rev_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `subcat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
