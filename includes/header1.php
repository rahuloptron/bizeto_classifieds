
	<header>
			<!-- LOGO -->
			<a href="/index.html">
				<figure class="logo">
					<img src="../images/logo.png" alt="logo">
				</figure>
			</a>
			<!-- /LOGO -->

			<!-- MOBILE MENU HANDLER -->
			<div class="mobile-menu-handler left primary">
				<img src="../images/pull-icon.png" alt="pull-icon">
			</div>
			<!-- /MOBILE MENU HANDLER -->

			<!-- LOGO MOBILE -->
			<a href="/index.html">
				<figure class="logo-mobile">
					<img src="../images/logo.png" alt="logo-mobile">
				</figure>
			</a>
			<!-- /LOGO MOBILE -->

			
			

			<div class="user-board">

<?php

	if(isset($_SESSION['user_id']))


	 { 
			$_SESSION['user_id'];

			$user_name = $_SESSION['user_name'];

	 	?>
				
				<div class="user-quickview">
					<!-- USER AVATAR -->
					<a href="author-profile.html">
					<div class="outer-ring">
						<div class="inner-ring"></div>
						<figure class="user-avatar">
							<img src="../images/avatars/avatar_01.jpg" alt="avatar">
						</figure>
					</div>
					</a>
					<!-- /USER AVATAR -->

					<!-- USER INFORMATION -->
					
					<!-- SVG ARROW -->
					<svg class="svg-arrow">
						<use xlink:href="#svg-arrow"></use>
					</svg>
					<!-- /SVG ARROW -->
					<p class="user-money"><?php echo ucfirst($user_name) ?></p>
					<!-- /USER INFORMATION -->

					<!-- DROPDOWN -->
					<ul class="dropdown small hover-effect closed">
						<li class="dropdown-item">
							<div class="dropdown-triangle"></div>
						<a href="/user/index.php">Account</a>
						</li>
						<li class="dropdown-item">
							<div class="dropdown-triangle"></div>
						<a href="/user/ad-post.php">Create New Ad</a>
						</li>
						<li class="dropdown-item">
								<a href="/logout.php">Logout</a>

						</li>
						
					</ul>
					<!-- /DROPDOWN -->
				</div>
				<?php } else{ ?>
				
				<div class="account-actions">
					<a href="/user/login.php" class="button secondary">Login</a>
					<a href="/user/register.php" class="button primary">Register</a>
				</div>

			<?php } ?>
				
			</div>

	
			<!-- /USER BOARD -->
		</header>