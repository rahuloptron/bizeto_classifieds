<?php

session_start();

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

$adid = $_GET['ad_id'];



include 'includes/config.php';

$query = "SELECT * from ad_table where ad_id = '$adid' AND approve_status = '1' ";

$data = mysqli_query($dbc,$query) or die(mysqli_error($dbc));

if (mysqli_num_rows($data) == 0) {

	echo  "No Found Data Please Try Again";

	}else{


		while($row = mysqli_fetch_array($data)){ 

?>


<!DOCTYPE html>
<html lang="en">
<head>
		<?php include 'includes/head.php' ?>
		<link rel="stylesheet" href="../css/vendor/magnific-popup.css">
	<!-- <link rel="stylesheet" href="../css/vendor/magnific-popup.css"> -->
	<link rel="icon" href="favicon.ico">
	<title><?php echo $row['ad_title'] ?></title>

</head>
<body>

	<div id="new-message-popup" class="form-popup new-message mfp-hide">
		<!-- FORM POPUP CONTENT -->
		<div class="form-popup-content">
			<h4 class="popup-title">Enquiry Form</h4>
			<!-- LINE SEPARATOR -->
			<hr class="line-separator">
			<!-- /LINE SEPARATOR -->
			<form id="formID" class="new-message-form" method="post">
				<!-- INPUT CONTAINER -->

				
				<input type="hidden" value="<?php echo $adid ?>" id="adid">
				<input type="hidden" value="<?php echo $row['ad_title'] ?>" id="adtitle">
				<input type="hidden" value="<?php echo $row['user_id'] ?>" id="userid">
				
				<div class="input-container">
					<label for="subject" class="rl-label b-label required">Contact Name:</label>
					<input type="text" id="cname" name="cname" placeholder="Enter Contact Name here...">
				</div>
				<!-- INPUT CONTAINER -->

				<div class="input-container">
					<label for="subject" class="rl-label b-label required">Mobile Number (Whatsapp):</label>
					<input type="text" id="cnumber" name="cnumber" placeholder="Enter Contact Number here...">
				</div>

				<div class="input-container">
					<label for="message" class="rl-label b-label required">Your Message:</label>
					<textarea id="cmessage" name="cmessage" placeholder="Write your message here...">I am Interested in <?php echo $row['ad_title'] ?> </textarea>
				</div>
				<!-- INPUT CONTAINER -->
				<span><a id="showError" class="showerror"></a></span>
				<button type="button" id="submitenquiry" name="submitenquiry" class="button mid dark">Send Enquiry</button>
				 
			</form>
		</div>
		<!-- /FORM POPUP CONTENT -->
	</div>	
	

	<!-- HEADER -->
	<div class="header-wrap">
		<?php include 'includes/header1.php' ?>
	</div>
	<!-- /HEADER -->

	<!-- SIDE MENU -->
		<?php include 'includes/mobile-menu.php' ?>
	<!-- /SIDE MENU -->

	

	<!-- MAIN MENU -->
		<?php include 'includes/menu-dark1.php' ?>
	<!-- /MAIN MENU -->

	<!-- SECTION HEADLINE -->

	

	<!-- SECTION -->
	<div class="section-wrap">
		<div class="section">
			<!-- SIDEBAR -->
			
			<!-- /SIDEBAR -->

			<!-- CONTENT -->
			<div class="content left">


				<!-- POST -->
				<article class="post">


					<!-- POST PARAGRAPH -->
						

					<!-- POST IMAGE -->
					<div class="post-image">
						<figure class="product-preview-image large liquid">
							<img src="../user/upload/<?php


									if($row['ad_image1'] == ''){

										echo "not-found.png";

									}else{

									 echo $row['ad_image1'];

									}

									  ?>" alt="">
						</figure>
						<!-- SLIDE CONTROLS -->
						
					</div>
					
					<!-- POST CONTENT -->
					<div class="post-content">
						<!-- POST PARAGRAPH -->
						<div class="post-paragraph">
							<h3 class="post-title">
								

								<?php


									if($row['ad_title'] == ''){

										echo "NA";

									}else{

									 echo $row['ad_title'];

									}

									  ?>
							</h3>

						</div>

						<div class="meta-line">


								
									<p class="category primary">Location :</p>
							
								<!-- METADATA -->
								<div class="metadata">
									
									<div class="meta-item">
										<p>
											
											<?php


									if($row['city'] == ''){

										echo "NA";

									}else{

									 echo $row['city'];

									}

									  ?>
										</p>
									</div>
									<!-- /META ITEM -->
								</div>


							
								<!-- /METADATA -->
								<p>Posted Date: <?php 

									$dt = new DateTime($row['post_date']);

									echo $dt->format('d-m-y');

								 ?></p>


						</div>

						<div class="post-paragraph">
						
						
						<p><?php echo $row['ad_desc']; ?></p>
					
						<hr class="line-separator"><br><br>
						<p>Contact Name:-  <?php


									if($row['contact_name'] == ''){

										echo "NA";

									}else{

									 echo $row['contact_name'];

									}

									  ?>
							
						</p>
						<p>Email Id:-  
							<?php


									if($row['contact_email'] == ''){

										echo "NA";

									}else{

									 echo $row['contact_email'];

									}

									  ?>
						</p>
						<p>Mobile Number:- 
							<?php


									if($row['contact_mobile'] == ''){

										echo "NA";

									}else{

									 echo $row['contact_mobile'];

									}

									  ?>
						</p>
						

						<p>Views:- </p>
						

						</div>

						
					</div>

					
				
					<hr class="line-separator">
					
					<!-- SHARE -->
					<div class="share-links-wrap">
						<p class="text-header small">Share this:</p>
						<!-- SHARE LINKS -->
						<ul class="share-links hoverable">
							<li><a href="#" class="fb"></a></li>
							<li><a href="#" class="twt"></a></li>
							<li><a href="#" class="db"></a></li>
							<li><a href="#" class="rss"></a></li>
							<li><a href="#" class="gplus"></a></li>
						</ul>
						<!-- /SHARE LINKS -->
					</div>


					<!-- /SHARE -->
				</article>
				<!-- /POST -->


			</div>
			<!-- CONTENT -->

			<div class="sidebar right">

				<div class="sidebar-item product-info">
					<h4>Posted By</h4>
					<hr class="line-separator">
					<!-- INFORMATION LAYOUT -->
					<div class="information-layout">
						<!-- INFORMATION LAYOUT ITEM -->
						<div class="information-layout-item">
							<p class="text-header">Name:</p>
							<p>
								
								<?php


									if($row['contact_name'] == ''){

										echo "NA";

									}else{

									 echo $row['contact_name'];

									}

									  ?>
							</p>
						</div>
						<!-- /INFORMATION LAYOUT ITEM -->

						<!-- INFORMATION LAYOUT ITEM -->
						<div class="information-layout-item">
							<p class="text-header">Contact:</p>
							<p>
								
								<?php


									if($row['contact_mobile'] == ''){

										echo "NA";

									}else{

									 echo $row['contact_mobile'];

									}

									  ?>
							</p>
						</div>
						<!-- /INFORMATION LAYOUT ITEM -->

						<!-- INFORMATION LAYOUT ITEM -->
						<div class="information-layout-item">
							<p class="text-header">Email:</p>
							<p>
								
								<?php


									if($row['contact_email'] == ''){

										echo "NA";

									}else{

									 echo $row['contact_email'];

									}

									  ?>
							</p>
						</div>
						<!-- /INFORMATION LAYOUT ITEM -->

						<!-- INFORMATION LAYOUT ITEM -->
						<div class="information-layout-item">
							<p class="text-header">Location:</p>
							<p>
								
								<?php


									if($row['city'] == ''){

										echo "NA";

									}else{

									 echo $row['city'];

									}

									  ?>
							</p>
						</div>
						<!-- /INFORMATION LAYOUT ITEM -->

					</div>
					<!-- INFORMATION LAYOUT -->
				</div>

			
          
           
				<a href="#new-message-popup" class="button big primary wcart open-new-message">Enquiry Now</a>
        
				<a href="https://api.whatsapp.com/send?phone=91<?php echo $row['contact_mobile']; ?>&text=I%20want%20<?php echo $row['ad_title'] ?> " class="button big primary wcart" target="_blank">Contact Me</a>

			

			
		</div>
	</div>

	<?php	} ?>

	<!-- /SECTION -->

	<!-- FOOTER -->
	<?php include 'includes/footer.php' ?>
	<!-- /FOOTER -->

		<?php	} ?>
	<div class="shadow-film closed"></div>

<!-- SVG ARROW -->
<svg style="display: none;">	
	<symbol id="svg-arrow" viewBox="0 0 3.923 6.64014" preserveAspectRatio="xMinYMin meet">
		<path d="M3.711,2.92L0.994,0.202c-0.215-0.213-0.562-0.213-0.776,0c-0.215,0.215-0.215,0.562,0,0.777l2.329,2.329
			L0.217,5.638c-0.215,0.215-0.214,0.562,0,0.776c0.214,0.214,0.562,0.215,0.776,0l2.717-2.718C3.925,3.482,3.925,3.135,3.711,2.92z"/>
	</symbol>
</svg>
<!-- /SVG ARROW -->

<!-- SVG STAR -->
<svg style="display: none;">
	<symbol id="svg-star" viewBox="0 0 10 10" preserveAspectRatio="xMinYMin meet">	
		<polygon points="4.994,0.249 6.538,3.376 9.99,3.878 7.492,6.313 8.082,9.751 4.994,8.129 1.907,9.751 
	2.495,6.313 -0.002,3.878 3.45,3.376 "/>
	</symbol>
</svg>
<!-- /SVG STAR -->

<!-- SVG PLUS -->
<svg style="display: none;">
	<symbol id="svg-plus" viewBox="0 0 13 13" preserveAspectRatio="xMinYMin meet">
		<rect x="5" width="3" height="13"/>
		<rect y="5" width="13" height="3"/>
	</symbol>
</svg>
<!-- /SVG PLUS -->

<!-- SVG CHECK -->
<svg style="display: none;">
	<symbol id="svg-check" viewBox="0 0 15 12" preserveAspectRatio="xMinYMin meet">
		<polygon points="12.45,0.344 5.39,7.404 2.562,4.575 0.429,6.708 3.257,9.536 3.257,9.536 
			5.379,11.657 14.571,2.465 "/>
	</symbol>
</svg>

<script src="../js/vendor/jquery-3.1.0.min.js"></script>
<!-- Magnific Popup -->
<script src="../js/vendor/jquery.magnific-popup.min.js"></script>
<!-- XM Pie Chart -->
<script src="../js/vendor/jquery.xmpiechart.min.js"></script>
<!-- Side Menu -->
<script src="../js/side-menu.js"></script>
<!-- Dashboard Header -->
<script src="../js/dashboard-header.js"></script>
<!-- Dashboard Inbox -->
<script src="../js/dashboard-inbox.js"></script>
<!-- Inbox Messages -->
<script src="../js/inbox-messages.js"></script>

<!-- Tooltipster -->
<script src="../js/vendor/jquery.tooltipster.min.js"></script>
<!-- ImgLiquid -->
<script src="../js/vendor/imgLiquid-min.js"></script>
<!-- XM Tab -->
<script src="../js/vendor/jquery.xmtab.min.js"></script>
<!-- Tweet -->
<script src="../js/vendor/twitter/jquery.tweet.min.js"></script>
<!-- Side Menu -->

<!-- Liquid -->
<script src="../js/liquid.js"></script>
<!-- Checkbox Link -->
<script src="../js/checkbox-link.js"></script>
<!-- Image Slides -->
<script src="../js/image-slides.js"></script>
<!-- Post Tab -->
<script src="../js/post-tab.js"></script>
<!-- XM Accordion -->
<script src="../js/vendor/jquery.xmaccordion.min.js"></script>
<!-- XM Pie Chart -->
<!-- Item V1 -->
<script src="../js/item-v1.js"></script>
<!-- Tooltip -->
<script src="../js/tooltip.js"></script>
<!-- User Quickview Dropdown -->
<script src="../js/user-board.js"></script>
<!-- Footer -->
<script src="../js/footer.js"></script>

<script src="/form.js"></script>

</body>
</html>