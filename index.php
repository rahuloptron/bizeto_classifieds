<?php


session_start();


include 'includes/config.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'includes/head1.php' ?>
	    <link rel="stylesheet" href="scroll.css">
	<title>Bizeto.com</title>
</head>
<body>

	<!-- HEADER -->
	<div class="header-wrap">
			<?php include 'includes/header1.php' ?>
	</div>
	<!-- /HEADER -->

	<!-- SIDE MENU -->
	<?php include 'includes/mobile-menu.php' ?>
	<!-- /SIDE MENU -->

	<!-- SIDE MENU -->
	
	<!-- /SIDE MENU -->

	<!-- MAIN MENU -->
	<?php include 'includes/menu-dark1.php' ?>
	<!-- /MAIN MENU -->

	<!-- SECTION HEADLINE -->
	
	<!-- /SECTION HEADLINE -->

	<!-- SIDEBAR NAV -->
	

	<!-- SECTION -->
	<div class="section-wrap">
		<div class="section">

			<!-- PRODUCT SHOWCASE -->
			<div class="product-showcase">
				<!-- PRODUCT LIST -->

				<div class="product-list grid column4-wrap">
<div class="tutorial_list">
                 
					<?php 


            $query = mysqli_query($dbc, "SELECT * FROM ad_table ORDER BY ad_id DESC LIMIT 12");

               $rowCount = mysqli_num_rows($query);

    			if($rowCount > 0){ 


				 while($row = mysqli_fetch_assoc($query)){ 

						$tutorial_id = $row['ad_id'];

						?>
                        
						 <div class="product-item column">

                            <a href="javascript:void(0);">
                            <!-- PRODUCT PREVIEW ACTIONS -->
                            <div class="product-preview-actions">
                                <!-- PRODUCT PREVIEW IMAGE -->
                                <figure class="product-preview-image">
                                    <img src="user/upload/<?php 

                                    if($row['ad_image'] == ''){

                                        echo "not-found.png";

                                    }else{

                                     echo $row['ad_image'];

                                    }

                                      ?>" alt="product-image">
                                </figure>
                                
                            </div>
                            <!-- /PRODUCT PREVIEW ACTIONS -->

                            <!-- PRODUCT INFO -->
                            <div class="product-info">
                                <a href="ads/<?php echo $row['ad_id']; ?>"><p class="text-header">
                                        
                                    <?php


                                    if($row['ad_title'] == ''){

                                        echo "NA";

                                    }else{

                                     echo $row['ad_title'];

                                    }

                                      ?>


                                    </p></a>
                                    
                                
                                <p class="product-description"><?php 


                                $string1 =  $row['ad_desc'];

                                $string = strip_tags($string1);

                                if (strlen($string) > 70) {

                                    // truncate string
                                    $stringCut = substr($string, 0, 70);

                                  
                                   $string = substr($stringCut, 0, strrpos($stringCut, ' ')); 


                                }
                            
                                    if($string == ''){

                                        echo "NA";

                                    }else{

                                     echo $string;

                                    }

                                    


                                 ?></p>
                                
                            </div>
                            <!-- /PRODUCT INFO -->
                            <hr class="line-separator">

                            <!-- USER RATING -->
                            <div class="user-rating">
                                
                                <a href="#">
                                    <p class="text-header tiny">
                                        
                                    <?php


                                    if($row['contact_name'] == ''){

                                        echo "NA";

                                    }else{

                                     echo $row['contact_name'];

                                    }

                                      ?>




                                    </p>
                                </a>
                                
                            </div>
                          </a>  <!-- /USER RATING -->
                        </div>
					<?php } ?>
	   <div class="show_more_main" id="show_more_main<?php echo $tutorial_id; ?>">
        <span id="<?php echo $tutorial_id; ?>" class="show_more" title="Load more posts">Show more</span>
        <span class="loding" style="display: none;"><span class="loding_txt">Loading....</span></span>
    </div>

    <?php } ?>
		  </div>		
                 </div>
				<!-- /PRODUCT LIST -->
			</div>
			<!-- /PRODUCT SHOWCASE -->

			
		</div>
	</div>
	<!-- /SECTION -->

	<!-- FOOTER -->
	<?php include 'includes/footer1.php' ?>
	<!-- /FOOTER -->

	<div class="shadow-film closed"></div>

<!-- SVG ARROW -->
<svg style="display: none;">	
	<symbol id="svg-arrow" viewBox="0 0 3.923 6.64014" preserveAspectRatio="xMinYMin meet">
		<path d="M3.711,2.92L0.994,0.202c-0.215-0.213-0.562-0.213-0.776,0c-0.215,0.215-0.215,0.562,0,0.777l2.329,2.329
			L0.217,5.638c-0.215,0.215-0.214,0.562,0,0.776c0.214,0.214,0.562,0.215,0.776,0l2.717-2.718C3.925,3.482,3.925,3.135,3.711,2.92z"/>
	</symbol>
</svg>
<!-- /SVG ARROW -->

<!-- SVG STAR -->
<svg style="display: none;">
	<symbol id="svg-star" viewBox="0 0 10 10" preserveAspectRatio="xMinYMin meet">	
		<polygon points="4.994,0.249 6.538,3.376 9.99,3.878 7.492,6.313 8.082,9.751 4.994,8.129 1.907,9.751 
	2.495,6.313 -0.002,3.878 3.45,3.376 "/>
	</symbol>
</svg>
<!-- /SVG STAR -->

<!-- SVG PLUS -->
<svg style="display: none;">
	<symbol id="svg-plus" viewBox="0 0 13 13" preserveAspectRatio="xMinYMin meet">
		<rect x="5" width="3" height="13"/>
		<rect y="5" width="13" height="3"/>
	</symbol>
</svg>
<!-- /SVG PLUS -->

<!-- jQuery -->
<script src="js/vendor/jquery-3.1.0.min.js"></script>
<!-- JRange -->
<script src="js/vendor/jquery.range.min.js"></script>
<!-- Tooltipster -->
<script src="js/vendor/jquery.tooltipster.min.js"></script>
<!-- Tweet -->
<script src="js/vendor/twitter/jquery.tweet.min.js"></script>
<!-- Side Menu -->
<script src="js/side-menu.js"></script>
<!-- Tooltip -->
<script src="js/tooltip.js"></script>
<!-- User Quickview Dropdown -->
<script src="js/user-board.js"></script>
<!-- Shop -->
<script src="js/shop.js"></script>
<!-- Footer -->
<script src="js/footer.js"></script>

<script src="/scroll.js"></script>
</body>
</html>