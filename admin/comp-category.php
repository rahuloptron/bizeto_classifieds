<?php
session_start();

if(!isset($_SESSION["adminemail"])&&($_SESSION["adminemail"]==''))
{
  header("Location: login.php");
  
}
else
{

  include 'config.php';

  $query = "SELECT * from comp_categories";

  $data = mysqli_query($dbc,$query)or die(mysqli_error($dbc));


  ?>


<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Categories</title>

<!-- Bootstrap Core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="vendor/morrisjs/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>

<body>

<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>


</div>
<!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right">

<!-- /.dropdown -->


<!-- /.dropdown -->
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">
<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
</a>
<ul class="dropdown-menu dropdown-user">
  

<li><a href="#"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['adminname']; ?></a>
</li>

<li class="divider"></li>
<li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
</li>
</ul>
<!-- /.dropdown-user -->
</li>
<!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->

<div class="navbar-default sidebar" role="navigation">
<div class="sidebar-nav navbar-collapse">
<ul class="nav" id="side-menu">

<li>
<a href="index.php" id="tabdashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
</li>

<li>
<a href="users.php" id="tabuser"><i class="fa fa-table fa-fw"></i> Users</a>
</li>
<li>
<a href="posts.php" id="tabpost"> <i class="fa fa-table fa-fw"></i> Ads</a>
</li>
<li>
<a href="companies.php" id="tabpost"> <i class="fa fa-table fa-fw"></i> Companies</a>
</li>
<li>
<a href="category.php" id="tabcategory"><i class="fa fa-table fa-fw"></i> Category</a>
</li>
<li>
<a href="comp-category.php" id="tabcategory"><i class="fa fa-table fa-fw"></i>Company Category</a>
</li>
<li>
<a href="ad-post.php" id="tabcategory"><i class="fa fa-edit fa-fw"></i> Ad Post</a>
</li>
<li>
<a href="company-post.php" id="tabcategory"><i class="fa fa-edit fa-fw"></i> Company Post</a>
</li>



</ul>
</div>
<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>

<div id="page-wrapper">
  <h2>Categories</h2><br>
 
  <table class="table table-bordered">
 
  
    <thead>
      <tr>
        <th>ID</th>
        <th>Category</th>
        <th>Seo Title</th>
        <th>Seo Url</th>
        <th>Image</th>
         <th>Edit</th>
       
      </tr>
    </thead>
    <tbody>
      <tr>
        <?php   while($row = mysqli_fetch_array($data)){ ?>

        <td><?php echo $row['cat_id']; ?></td>
        <td><?php echo $row['cat_name']; ?></td>
         <td><?php echo $row['seo_title']; ?></td>
          <td><?php echo $row['seo_url']; ?></td>
           <td><img src="../user/upload/category/<?php echo $row['cat_image']; ?>" alt=""></td>
           <td><a href="edit-comp-category.php?cat_id=<?php echo $row['cat_id']; ?>">Edit</a></td>
       
      </tr>
    </tbody>

   <?php } ?>
  </table>
<ul class="nav" id="side-menu">
<li><a href="comp-category-create.php"><i class="fa fa-edit fa-fw"></i> Create New Category</a>
</li>
   </ul>  
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="vendor/raphael/raphael.min.js"></script>
<script src="vendor/morrisjs/morris.min.js"></script>
<script src="data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

<?php } ?>
</body>

</html>
<script>
$(document).ready(function(){
// $("#divpost").hide();
$("#tabpost").click(function(){
$("#divpost").show();
});
$("#show").click(function(){
$("p").show();
});
});


</script>
