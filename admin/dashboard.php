<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="../css/vendor/simple-line-icons.css">
	<link rel="stylesheet" href="../css/vendor/magnific-popup.css">
	<link rel="stylesheet" href="../css/style.css">
	<!-- favicon -->
	<link rel="icon" href="favicon.ico">
	<title>Emerald Dragon | Dashboard</title>
</head>
<body>

	<!-- FORM POPUP -->
	<div id="new-message-popup" class="form-popup new-message mfp-hide">
		<!-- FORM POPUP CONTENT -->
		<div class="form-popup-content">
			<h4 class="popup-title">Write a New Message</h4>
			<!-- LINE SEPARATOR -->
			<hr class="line-separator">
			<!-- /LINE SEPARATOR -->
			<form class="new-message-form">
				<!-- INPUT CONTAINER -->
				<div class="input-container field-add">
					<label for="mailto" class="rl-label b-label required">To:</label>
					<label for="mailto" class="select-block">
							<select name="mailto" id="mailto">
								<option value="0">Select from the authors you follow...</option>
								<option value="1">Vynil Brush</option>
								<option value="2">Jenny_Block</option>
							</select>
							<!-- SVG ARROW -->
							<svg class="svg-arrow">
								<use xlink:href="#svg-arrow"></use>
							</svg>
							<!-- /SVG ARROW -->
					</label>
					<div class="button dark-light add-field">
						<!-- SVG PLUS -->
						<svg class="svg-plus">
							<use xlink:href="#svg-plus"></use>
						</svg>
						<!-- /SVG PLUS -->
					</div>
				</div>
				<!-- /INPUT CONTAINER -->

				<!-- INPUT CONTAINER -->
				<div class="input-container">
					<label for="subject" class="rl-label b-label">Subject:</label>
					<input type="text" id="subject" name="subject" placeholder="Enter your subject here...">
				</div>
				<!-- INPUT CONTAINER -->

				<!-- INPUT CONTAINER -->
				<div class="input-container">
					<label for="message" class="rl-label b-label required">Your Message:</label>
					<textarea id="message" name="message" placeholder="Write your message here..."></textarea>
				</div>
				<!-- INPUT CONTAINER -->

				<button class="button mid dark">Send <span class="primary">Message</span></button>
			</form>
		</div>
		<!-- /FORM POPUP CONTENT -->
	</div>
	<!-- /FORM POPUP -->

	<!-- SIDE MENU -->
	<div id="dashboard-options-menu" class="side-menu dashboard left closed">
        <!-- SVG PLUS -->
		<svg class="svg-plus">
			<use xlink:href="#svg-plus"></use>
		</svg>
		<!-- /SVG PLUS -->
        
		<!-- SIDE MENU HEADER -->
		<div class="side-menu-header">
			<!-- USER QUICKVIEW -->
			<div class="user-quickview">
				<!-- USER AVATAR -->
				<a href="author-profile.html">
				<div class="outer-ring">
					<div class="inner-ring"></div>
					<figure class="user-avatar">
						<img src="../../images/avatars/avatar_01.jpg" alt="avatar">
					</figure>
				</div>
				</a>
				<!-- /USER AVATAR -->

				<!-- USER INFORMATION -->
				<p class="user-name">Johnny Fisher</p>
				<p class="user-money">$745.00</p>
				<!-- /USER INFORMATION -->
			</div>
			<!-- /USER QUICKVIEW -->
		</div>
		<!-- /SIDE MENU HEADER -->

		<!-- SIDE MENU TITLE -->
		<p class="side-menu-title">Your Account</p>
		<!-- /SIDE MENU TITLE -->

		<!-- DROPDOWN -->
		<ul class="dropdown dark hover-effect interactive">
			<!-- DROPDOWN ITEM -->
			<li class="dropdown-item">
				<a href="dashboard-settings.html">
                    <span class="sl-icon icon-settings"></span>
                    Account Settings
                </a>
			</li>
			<!-- /DROPDOWN ITEM -->

			<!-- DROPDOWN ITEM -->
			<li class="dropdown-item">
				<a href="dashboard-notifications.html">
                    <span class="sl-icon icon-star"></span>
                    New Enquiries
                </a>
                <!-- PIN -->
                <span class="pin soft-edged big primary">49</span>
                <!-- /PIN -->
			</li>
			<!-- /DROPDOWN ITEM -->

			<!-- DROPDOWN ITEM -->
			<li class="dropdown-item active interactive">
				<a href="#">
                    <span class="sl-icon icon-envelope"></span>
                    Ads
                    <!-- SVG ARROW -->
					<svg class="svg-arrow">
						<use xlink:href="#svg-arrow"></use>
					</svg>
					<!-- /SVG ARROW -->
				</a>

				<!-- INNER DROPDOWN -->
				<ul class="inner-dropdown open">
					<!-- INNER DROPDOWN ITEM -->
					<li class="inner-dropdown-item">
						<a href="dashboard-inbox.html">Show Ads</a>
						<!-- PIN -->
						<span class="pin soft-edged secondary">2</span>
						<!-- /PIN -->
					</li>
					<!-- /INNER DROPDOWN ITEM -->

					<!-- INNER DROPDOWN ITEM -->
					<li class="inner-dropdown-item">
						<a href="dashboard-inbox-v2.html">Create Ads</a>
					</li>
					<!-- /INNER DROPDOWN ITEM -->

					<!-- INNER DROPDOWN ITEM -->
					<li class="inner-dropdown-item">
						<a href="dashboard-openmessage.html">Edit Ads</a>
					</li>
					
					
				</ul>
				<!-- INNER DROPDOWN -->

                <!-- PIN -->
                <span class="pin soft-edged big secondary">!</span>
                <!-- /PIN -->
			</li>

			<li class="dropdown-item active interactive">
				<a href="#">
                    <span class="sl-icon icon-envelope"></span>
                    Companies
                    <!-- SVG ARROW -->
					<svg class="svg-arrow">
						<use xlink:href="#svg-arrow"></use>
					</svg>
					<!-- /SVG ARROW -->
				</a>

				<!-- INNER DROPDOWN -->
				<ul class="inner-dropdown open">
					<!-- INNER DROPDOWN ITEM -->
					<li class="inner-dropdown-item">
						<a href="dashboard-inbox.html">Show Companies</a>
						<!-- PIN -->
						<span class="pin soft-edged secondary">2</span>
						<!-- /PIN -->
					</li>
					<!-- /INNER DROPDOWN ITEM -->

					<!-- INNER DROPDOWN ITEM -->
					<li class="inner-dropdown-item">
						<a href="dashboard-inbox-v2.html">Create Companies</a>
					</li>
					<!-- /INNER DROPDOWN ITEM -->

					<!-- INNER DROPDOWN ITEM -->
					<li class="inner-dropdown-item">
						<a href="dashboard-openmessage.html">Edit Companies</a>
					</li>
					
					
				</ul>
				<!-- INNER DROPDOWN -->

                <!-- PIN -->
                <span class="pin soft-edged big secondary">!</span>
                <!-- /PIN -->
			</li>

			<li class="dropdown-item active interactive">
				<a href="#">
                    <span class="sl-icon icon-envelope"></span>
                    Categories
                    <!-- SVG ARROW -->
					<svg class="svg-arrow">
						<use xlink:href="#svg-arrow"></use>
					</svg>
					<!-- /SVG ARROW -->
				</a>

				<!-- INNER DROPDOWN -->
				<ul class="inner-dropdown open">
					<!-- INNER DROPDOWN ITEM -->
					<li class="inner-dropdown-item">
						<a href="dashboard-inbox.html">Show Categories</a>
						<!-- PIN -->
						<span class="pin soft-edged secondary">2</span>
						<!-- /PIN -->
					</li>
					<!-- /INNER DROPDOWN ITEM -->

					<!-- INNER DROPDOWN ITEM -->
					<li class="inner-dropdown-item">
						<a href="dashboard-inbox-v2.html">Create Categories</a>
					</li>
					<!-- /INNER DROPDOWN ITEM -->

					<!-- INNER DROPDOWN ITEM -->
					<li class="inner-dropdown-item">
						<a href="dashboard-openmessage.html">Edit Categories</a>
					</li>
					
					
				</ul>
				<!-- INNER DROPDOWN -->

                <!-- PIN -->
                <span class="pin soft-edged big secondary">!</span>
                <!-- /PIN -->
			</li>
			
		</ul>
		<!-- /DROPDOWN -->


	

        <a href="#" class="button medium secondary">Logout</a>
	</div>
	<!-- /SIDE MENU --<div class="dashboard-body">>

    <!-- DASHBOARD BODY -->
    <div class="dashboard-body">
        <!-- DASHBOARD HEADER -->
        <div class="dashboard-header retracted">
            <!-- DB CLOSE BUTTON -->
         
            <!-- DB CLOSE BUTTON -->

			<!-- DB OPTIONS BUTTON -->
            <div class="db-options-button">
               <img src="../images/dashboard/db-list-right.png" alt="db-list-right">
			   <img src="../images/dashboard/close-icon.png" alt="close-icon">
            </div>
           
        </div>
        <!-- DASHBOARD HEADER -->

        <!-- DASHBOARD CONTENT -->
        <div class="dashboard-content">
            <!-- HEADLINE -->
            <div class="headline buttons two primary">
                <h4>Your Inbox (36)</h4>
				
            </div>
            <!-- /HEADLINE -->

			<!-- INBOX MESSAGES -->
			<div class="inbox-messages">
		
				<!-- INBOX MESSAGE -->

				<!-- INBOX MESSAGE -->
				<div class="inbox-message">
					<div class="inbox-message-actions">
						<!-- CHECKBOX -->
						<input type="checkbox" id="msg8" name="msg8">
						<label for="msg8" class="label-check">
							<span class="checkbox primary"><span></span></span>
						</label>
						<!-- /CHECKBOX -->

						<!-- STARRED -->
						<div class="starred">
							<img src="../images/dashboard/star-empty.png" class="visible" alt="star-empty">
							<img src="../images/dashboard/star-filled.png" class="hidden" alt="star-filled">
						</div>
						<!-- /STARRED -->
					</div>
					
					<div class="inbox-message-author">
						<figure class="user-avatar">
							<img src="../images/avatars/avatar_05.jpg" alt="user-img">
						</figure>
						<p class="text-header">
							DaBebop
							<span class="message-icon icon-envelope-open"></span>
						</p>
					</div>

					<a href="dashboard-openmessage.html">
						<div class="inbox-message-content">
							<p class="text-header normal">Miniverse Mockup Inquiry</p>
							<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
						</div>
					</a>

					<div class="inbox-message-type">
						<span class="message-icon icon-envelope-open"></span>
					</div>

					<div class="inbox-message-date">
						<p>May 24th, 2013</p>
					</div>
				</div>
				<!-- INBOX MESSAGE -->

				<!-- PAGER -->
				<div class="pager-wrap">
					<div class="pager primary">
						<div class="pager-item"><p>1</p></div>
						<div class="pager-item active"><p>2</p></div>
						<div class="pager-item"><p>3</p></div>
						<div class="pager-item"><p>...</p></div>
						<div class="pager-item"><p>17</p></div>
					</div>
				</div>
				<!-- /PAGER -->
			</div>
			<!-- /INBOX MESSAGES -->
        </div>
        <!-- DASHBOARD CONTENT -->
    </div>
    <!-- /DASHBOARD BODY -->

	<div class="shadow-film closed"></div>

<!-- SVG ARROW -->
<svg style="display: none;">	
	<symbol id="svg-arrow" viewBox="0 0 3.923 6.64014" preserveAspectRatio="xMinYMin meet">
		<path d="M3.711,2.92L0.994,0.202c-0.215-0.213-0.562-0.213-0.776,0c-0.215,0.215-0.215,0.562,0,0.777l2.329,2.329
			L0.217,5.638c-0.215,0.215-0.214,0.562,0,0.776c0.214,0.214,0.562,0.215,0.776,0l2.717-2.718C3.925,3.482,3.925,3.135,3.711,2.92z"/>
	</symbol>
</svg>
<!-- /SVG ARROW -->

<!-- SVG PLUS -->
<svg style="display: none;">
	<symbol id="svg-plus" viewBox="0 0 13 13" preserveAspectRatio="xMinYMin meet">
		<rect x="5" width="3" height="13"/>
		<rect y="5" width="13" height="3"/>
	</symbol>
</svg>
<!-- /SVG PLUS -->

<!-- SVG MINUS -->
<svg style="display: none;">
	<symbol id="svg-minus" viewBox="0 0 13 13" preserveAspectRatio="xMinYMin meet">
		<rect y="5" width="13" height="3"/>
	</symbol>
</svg>
<!-- /SVG MINUS -->

<!-- jQuery -->
<script src="../js/vendor/jquery-3.1.0.min.js"></script>
<!-- Magnific Popup -->
<script src="../js/vendor/jquery.magnific-popup.min.js"></script>
<!-- XM Pie Chart -->
<script src="../js/vendor/jquery.xmpiechart.min.js"></script>
<!-- Side Menu -->
<script src="../js/side-menu.js"></script>
<!-- Dashboard Header -->
<script src="../js/dashboard-header.js"></script>
<!-- Dashboard Inbox -->
<script src="../js/dashboard-inbox.js"></script>
<!-- Inbox Messages -->
<script src="../js/inbox-messages.js"></script>
</body>
</html>