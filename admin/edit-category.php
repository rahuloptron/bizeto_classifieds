<?php
session_start();

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

if(!isset($_SESSION["adminemail"])&&($_SESSION["adminemail"]==''))
{
  header("Location: login.php");
  
}
else
{

	include 'config.php';

	$getid = $_GET['cat_id'];

$query = "SELECT * FROM categories where cat_id = '$getid'";

$data = mysqli_query($dbc,$query)or die(mysqli_error($dbc));


  ?>

<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Category Edit</title>

<!-- Bootstrap Core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="vendor/morrisjs/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>

<body>

<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="category.html">Back</a>

</div>
<!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right">

<!-- /.dropdown -->


<!-- /.dropdown -->
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">
<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
</a>
<ul class="dropdown-menu dropdown-user">


<li><a href="/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
</li>
</ul>
<!-- /.dropdown-user -->
</li>
<!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->

<div class="navbar-default sidebar" role="navigation">
<div class="sidebar-nav navbar-collapse">
<ul class="nav" id="side-menu">





</ul>
</div>
<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>

<div id="page-wrapper">
<br>
<!-- /.row -->
<div class="row">
<div class="col-lg-6" id="divpost">
<div class="panel panel-default">
<div class="panel-heading">
<i class="fa fa-bar-chart-o fa-fw"></i> Companies Listing


</div>
<!-- /.panel-heading -->
<div class="panel-body">
<form method="post" action="category-edit-back.php" enctype="multipart/form-data">
<?php	while($row = mysqli_fetch_array($data)){ ?>

<div class="form-group">
<label>Category Name</label>
<input class="form-control" value="<?php echo $row['cat_name']; ?>"  placeholder="Enter Your Category Name" maxlength="70" name="categoryname" >
</div>

<div class="form-group">
<label>Seo Url</label>
<input class="form-control" value="<?php echo $row['seourl']; ?>"  placeholder="Enter url with -" name="seourl">
</div>

<input type="hidden" name="getid" value="<?php echo $getid ?>">

<div class="form-group">
<label>Seo Title</label>
<input class="form-control" value="<?php echo $row['seotitle']; ?>"  placeholder="Enter Seo Title" maxlength="200" name="seotitle" >
</div>



<div class="form-group">
<label>Category Image</label>

<input type="hidden" name="imagename" value="<?php echo $row['cat_image']; ?>">

<img src="../user/upload/category/<?php echo $row['cat_image']; ?>" alt="">

<?php 


if($row['cat_image'] == ''){


}else{   ?>

	<a href="delete-image.php?imgid=<?php echo $getid; ?>&&imgname=<?php echo $row['cat_image']; ?> ">Delete</a>


<?php } ?>


<input class="form-control" type="file" name="FileToUpload" id="FileToUpload">
</div>


<div class="form-group">
<input class="btn-success" type="submit" class="btn btn-default" value="Update Category">
</div>

<?php } ?>
</form>
</div>
<!-- /.panel-body -->
</div>

</div>

</div>
<!-- /.row -->

<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="vendor/raphael/raphael.min.js"></script>
<script src="vendor/morrisjs/morris.min.js"></script>
<script src="data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
<script>
$(document).ready(function(){
// $("#divpost").hide();
$("#tabpost").click(function(){
$("#divpost").show();
});
$("#show").click(function(){
$("p").show();
});
});
<?php } ?>
</script>
