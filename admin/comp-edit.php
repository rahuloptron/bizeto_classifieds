<?php
session_start();

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

if(!isset($_SESSION["adminemail"])&&($_SESSION["adminemail"]==''))
{
  header("Location: login.php");
  
}
else
{

	include 'config.php';

	$getid = $_GET['id'];

$query = "SELECT * FROM companies where comp_id = '$getid'";

$data = mysqli_query($dbc,$query)or die(mysqli_error($dbc));


  ?>

<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Companies Listing</title>

<!-- Bootstrap Core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="vendor/morrisjs/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>

<body>

<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="companies.html">Back</a>

</div>
<!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right">

<!-- /.dropdown -->


<!-- /.dropdown -->
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">
<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
</a>
<ul class="dropdown-menu dropdown-user">


<li><a href="/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
</li>
</ul>
<!-- /.dropdown-user -->
</li>
<!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->

<div class="navbar-default sidebar" role="navigation">
<div class="sidebar-nav navbar-collapse">
<ul class="nav" id="side-menu">





</ul>
</div>
<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>

<div id="page-wrapper">
<br>
<!-- /.row -->
<div class="row">
<div class="col-lg-6" id="divpost">
<div class="panel panel-default">
<div class="panel-heading">
<i class="fa fa-bar-chart-o fa-fw"></i> Companies Listing


</div>
<!-- /.panel-heading -->
<div class="panel-body">
<form method="post" action="comp-back.php" enctype="multipart/form-data">
<?php	while($row = mysqli_fetch_array($data)){ ?>

<div class="form-group">
<label>Approve Status</label>
<select class="form-control"  name="approved" required>
  
 <?php $approved = $row['approve_status']; ?>
<option value="" <?php if (!empty($approved) && $approved == '' ) echo 'selected = "selected"'; ?>>Select</option>
<option value="1" <?php if (!empty($approved) && $approved == '1')  echo 'selected = "selected"'; ?>>Approved</option>
<option value="0" <?php if (!empty($approved) && $approved == '0')  echo 'selected = "selected"'; ?>>Pending</option>
</select>

</div>

<div class="form-group">
<label>Company Title</label>
<input class="form-control" value="<?php echo $row['comp_title']; ?>" placeholder="Enter Your Company Title" maxlength="70" name="companytitle" >
</div>


<div class="form-group">
<label>Company Description</label>
<textarea class="form-control" placeholder="Enter Your Short Description" rows="4" name="companydesc"><?php echo $row['comp_desc']; ?></textarea>
</div>

<div class="form-group">
<label>Listing Type</label>
<select class="form-control" id="comptype" name="comptype">
  <?php $comptype = $row['comp_type']; ?>
<option value="" <?php if (!empty($comptype) && $comptype == '' ) echo 'selected = "selected"'; ?>>Select</option>
<option value="Free" <?php if (!empty($comptype) && $comptype == 'Free')  echo 'selected = "selected"'; ?>>Free</option>
<option value="Premium" <?php if (!empty($comptype) && $comptype == 'Premium')  echo 'selected = "selected"'; ?>>Premium</option>
</select>
</div>

<div class="form-group">
<label>Contact Person</label>
<input class="form-control" value="<?php echo $row['contact_person']; ?>" placeholder="Enter Your Name" maxlength="200" name="contactperson" >
</div>
<div class="form-group">
<label>Contact Mobile Number</label>
<input type="text" class="form-control" value="<?php echo $row['contact_mobile']; ?>" pattern="[0-9]{10}" placeholder="Enter Your Mobile Number" name="companymobile" maxlength="10" >
</div>
<div class="form-group">
<label>Contact Email Id</label>
<input type="email" value="<?php echo $row['contact_email']; ?>" class="form-control" placeholder="Enter Your Email ID" maxlength="200" name="companyemail" >
</div>
<div class="form-group">
<label>Company Website</label>
<input class="form-control" value="<?php echo $row['comp_website']; ?>" placeholder="Enter Your Company Website" maxlength="50" name="companywebsite">
</div>
<div class="form-group">
<label>Company Address</label>
<textarea class="form-control" name="companyaddress" placeholder="Enter Your Address" rows="3"><?php echo $row['comp_address']; ?></textarea>
</div>
<input type="hidden" value="<?php echo $getid; ?>" name="getid">

<div class="form-group">
<label>Company Logo</label>
<input type="hidden" value="<?php echo $row['comp_logo']; ?>" name="imagelogo">

<?php 


if($row['comp_logo'] == ''){


}else{   ?>

<img src="/user/upload/logo/<?php echo $row['comp_logo']; ?>" alt="" style="width:200px; height:150px;">

	<a href="delete-company-image.php?imgid=<?php echo $getid; ?>&&imgname=<?php echo $row['comp_logo']; ?> ">Delete</a>


<?php } ?>
<input class="form-control" type="file" name="FileToUpload" id="FileToUpload">
</div>

<div class="form-group">
<label>Url Slug</label>
<input class="form-control" value="<?php echo $row['seo_url']; ?>" placeholder="Enter url with -" name="urlslug">
</div>

<div class="form-group">
<input class="btn-success" type="submit" class="btn btn-default" value="Update Company">
</div>

<?php } ?>
</form>
</div>
<!-- /.panel-body -->
</div>

</div>

</div>
<!-- /.row -->

<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="vendor/raphael/raphael.min.js"></script>
<script src="vendor/morrisjs/morris.min.js"></script>
<script src="data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
<script>
$(document).ready(function(){
// $("#divpost").hide();
$("#tabpost").click(function(){
$("#divpost").show();
});
$("#show").click(function(){
$("p").show();
});
});
<?php } ?>
</script>
