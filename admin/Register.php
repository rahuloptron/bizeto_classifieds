<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Event Registration Form</title>

        <!-- CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
	
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

		<script src="js/myscript.js"></script>
   

    </head>

    <body background="assets/img/backgrounds/1.jpg">

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                  
                    <div class="row" >
                        <div class="col-sm-3 col-sm-offset-3 form-box" style="position:fixed;top:5%">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h2>Registration</h2>
                            		
                        		</div>
                        		<div class="form-top-right">
                        			
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" onsubmit="return validateForm()" action="Register_back.php" method="post" name="myForm" class="login-form" >
			                    	
									<div class="form-group">
			                    		
			                        	<input type="hidden" name="eventid" value='<?php echo $eventid; ?>'>
										
			                        </div><div class="form-group">
			                    		<label class="sr-only" for="form-username">Name</label>
			                        	<input type="text" name="txtname" placeholder="Enter Your Full Name" class="form-username form-control" id="txtname" >
										<div id="error-name"></div>
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Mobile Number</label>
			                        	<input type="number" name="txtnumber" placeholder="Enter 10 Mobile Number" class="form-Number form-control" id="txtnumber" >
										<div id="error-number" ></div>
			                        </div>
									 <div class="form-group">
			                        	<label class="sr-only" for="form-password">Email ID</label>
			                        	<input type="email" name="txtemail" placeholder="Enter Your Email ID" class="form-Number form-control" id="txtemail" >
										<div id="error-email"></div>
			                        </div>
									
									<button type="submit" id="myBtn" class="btn" >Register</button>
									
									<a href="login.php" class="btn btn-link"><span style="color:red">Login</span> If You Have Your Account</a>
			                    </form>
		                    </div>
                        </div>
                    </div>
                  
                </div>
            </div>
            
        </div>


	<!--	<script src="assets/js/scripts.js"></script> -->
        
       
       <script>
function validateForm() {
	
    var name = document.forms["myForm"]["txtname"].value;
	var number = document.forms["myForm"]["txtnumber"].value;
	var email = document.forms["myForm"]["txtemail"].value;
	var phoneno = /^\d{10}$/;
	document.getElementById("error-name").innerHTML = "";
	document.getElementById("error-number").innerHTML = "";
	document.getElementById("error-email").innerHTML = "";
    if (name == "") {
    	document.getElementById("error-name").innerHTML = "<span style='color:red'>Name must be filled out</span>";
       //alert("Name must be filled out");
        return false;
    }
	else if (number == "") {
    	document.getElementById("error-number").innerHTML = "<span style='color:red'>Number must be filled out</span>";
       // alert("Name must be filled out");
        return false;
    }
	/*else if (!(number.value.match(phoneno)) {
    	document.getElementById("error-number").innerHTML = "<span style='color:red'>Number must be 10 Digit</span>";
       // alert("Name must be filled out");
        return false;
    } */

	else if (email == "") {
    	document.getElementById("error-email").innerHTML = "<span style='color:red'>Email Id must be filled out</span>";
       // alert("Name must be filled out");
        return false;
    }
	else if (email != null)  {
			
			
			 var xmlhttp = new XMLHttpRequest();
			xmlhttp.open("Get", "email-check.php?email="+email , false);
			xmlhttp.send(null);

			
			if(xmlhttp.responseText== 1)
			{
				document.getElementById("error-email").innerHTML="<span style='color:red'>Email Id already Register</span>";
				return false;
			}
			
		
        
    }
	else {
    	//document.getElementById("error-name").innerHTML = "Name must be filled out";
       // alert("Name must be filled out");
	   document.getElementById("myBtn").disabled = true;
        return true;
    }
}
</script>	   
      

        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
    </body>

</html>