<?php
session_start();

$_SESSION['url'] = $_SERVER['REQUEST_URI'];


if(!isset($_SESSION["adminemail"])&&($_SESSION["adminemail"]==''))
{
	header("Location: login.php");
	
}
else
{

   include 'config.php';

  $getid = $_GET['id'];



$query = "SELECT * FROM ad_table where ad_id = '$getid'";

$data = mysqli_query($dbc,$query)or die(mysqli_error($dbc));


  ?>


<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Upload Image Page</title>

<!-- Bootstrap Core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="vendor/morrisjs/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>

<body>

<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="posts.php">Back</a>

</div>
<!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right">

<!-- /.dropdown -->


<!-- /.dropdown -->
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">
<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
</a>
<ul class="dropdown-menu dropdown-user">


<li><a href="/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
</li>
</ul>
<!-- /.dropdown-user -->
</li>
<!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->

<div class="navbar-default sidebar" role="navigation">
<div class="sidebar-nav navbar-collapse">
<ul class="nav" id="side-menu">





</ul>
</div>
<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>

<div id="page-wrapper">
<br>
<!-- /.row -->
<div class="row">
<div class="col-lg-6" id="divpost">
<div class="panel panel-default">
<div class="panel-heading">
<i class="fa fa-bar-chart-o fa-fw"></i> Upload Product


</div>
<!-- /.panel-heading -->
<div class="panel-body">
<form method="post" action="edit-back.php" enctype="multipart/form-data">
  <?php while($row = mysqli_fetch_array($data)){ ?>


<div class="form-group">
<label>Approve Status</label>
<select class="form-control"  name="approved" required>
  
 <?php $approved = $row['approve_status']; ?>
<option value="" <?php if (!empty($approved) && $approved == '' ) echo 'selected = "selected"'; ?>>Select</option>
<option value="1" <?php if (!empty($approved) && $approved == '1')  echo 'selected = "selected"'; ?>>Approved</option>
<option value="0" <?php if (!empty($approved) && $approved == '0')  echo 'selected = "selected"'; ?>>Pending</option>
</select>

</div>

<input type="hidden" value="<?php echo $getid; ?>" name="getid">

<div class="form-group">
<label>Ad Title</label>
<input class="form-control" value="<?php echo $row['ad_title']; ?>" placeholder="Enter Your Ad Title" maxlength="70" name="adtitle" >
</div>
<div class="form-group">
<label>Ad Description</label>
<textarea class="form-control" placeholder="Enter Your Short Description" rows="4"  name="addesc" ><?php echo $row['ad_desc']; ?></textarea>
</div>



<div class="form-group">
<label>Listing Type</label>
<select class="form-control" id="adtype" name="adtype">
  <?php $adtype = $row['ad_type']; ?>
<option value="" <?php if (!empty($adtype) && $adtype == '' ) echo 'selected = "selected"'; ?>>Select</option>
<option value="Free" <?php if (!empty($adtype) && $adtype == 'Free')  echo 'selected = "selected"'; ?>>Free</option>
<option value="Premium" <?php if (!empty($adtype) && $adtype == 'Premium')  echo 'selected = "selected"'; ?>>Premium</option>
</select>
</div>

<div class="form-group">
<label>Select Category</label>
<select class="form-control"  name="categories" >
  <option value="">Select</option>

 <?php 


 $query1 = "SELECT * FROM categories";

  $data1 = mysqli_query($dbc,$query1)or die($dbc);

while($row1 = mysqli_fetch_array($data1)){ ?>


<option value="<?php echo $row1['cat_name']; ?>"><?php echo $row1['cat_name']; ?></option>
  
<?php } ?>
  </select>
</div>


<div class="form-group">
<label>Contact Name</label>
<input class="form-control" value="<?php echo $row['contact_name']; ?>" placeholder="Enter Your Name" maxlength="200" name="cname" >
</div>
<div class="form-group">
<label>Contact Mobile Number</label>
<input type="text" value="<?php echo $row['contact_mobile']; ?>" class="form-control" placeholder="Enter Your Mobile Number" name="cmobile" maxlength="10" >
</div>
<div class="form-group">
<label>Contact Email Id</label>
<input type="email" value="<?php echo $row['contact_email']; ?>" class="form-control" placeholder="Enter Your Email ID" maxlength="200" name="cemail" >
</div>
<!-- <div class="form-group">
<label>Company Name</label>
<input class="form-control" value="<?php echo $row['company_name']; ?>" placeholder="Enter Your Company Name" maxlength="200" name="companyname" >
</div>
<div class="form-group">
<label>City</label>
<input class="form-control" value="<?php echo $row['city']; ?>" placeholder="Enter Your City" maxlength="200" name="city" >
</div> -->

<div class="form-group">
<label>Thumb Image</label>

<input type="hidden" value="<?php echo $row['ad_image']; ?>" name="imagename">


<?php 


if($row['ad_image'] == ''){


}else{   ?>

<img src="/user/upload/<?php echo $row['ad_image'];?>" alt="" style="width:200px; height:150px;">

  <a href="delete-ad-image.php?imgid=<?php echo $getid; ?>&&imgname=<?php echo $row['ad_image']; ?> ">Delete</a>


<?php } ?>
<input class="form-control" type="file" name="FileToUpload" id="FileToUpload" >
</div>


<div class="form-group">
<label>Main Image</label>
<input type="hidden" value="<?php echo $row['ad_image1']; ?>" name="imagename1">

<?php 


if($row['ad_image1'] == ''){


}else{   ?>

  <img src="/user/upload/<?php echo $row['ad_image1']; ?>" alt="" style="width:200px; height:150px;">


  <a href="delete-ad-image1.php?imgid=<?php echo $getid; ?>&&imgname=<?php echo $row['ad_image1']; ?> ">Delete</a>


<?php } ?>

<input class="form-control" type="file" name="FileToUpload1" id="FileToUpload1">

</div> 


<div class="form-group">
<input class="btn-success" type="submit" class="btn btn-default" value="Update Ad">
</div>



<?php } ?>
</form>
</div>
<!-- /.panel-body -->
</div>

</div>

</div>
<!-- /.row -->

<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="vendor/raphael/raphael.min.js"></script>
<script src="vendor/morrisjs/morris.min.js"></script>
<script src="data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
<script>
$(document).ready(function(){
// $("#divpost").hide();
$("#tabpost").click(function(){
$("#divpost").show();
});
$("#show").click(function(){
$("p").show();
});
});

<?php } ?>
</script>
