<?php
session_start();
if(!isset($_SESSION["adminemail"])&&($_SESSION["adminemail"]==''))
{
	header("Location: login.php");
	
}
else
{

  include 'config.php';

  $query = "SELECT * FROM categories";

  $data = mysqli_query($dbc,$query)or die($dbc);


	
	?>

<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Ad Post</title>

<!-- Bootstrap Core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="vendor/morrisjs/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>

<body>

<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>


</div>
<!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right">

<!-- /.dropdown -->


<!-- /.dropdown -->
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">
<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
</a>
<ul class="dropdown-menu dropdown-user">
<li><a href="/index.html"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION["adminname"]; ?></a>
</li>

<li><a href="/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
</li>
</ul>
<!-- /.dropdown-user -->
</li>
<!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->

<div class="navbar-default sidebar" role="navigation">
<div class="sidebar-nav navbar-collapse">
<ul class="nav" id="side-menu">
<li>
<a href="index.php" id="tabdashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
</li>

<li>
<a href="users.php" id="tabuser"><i class="fa fa-table fa-fw"></i> Users</a>
</li>
<li>
<a href="posts.php" id="tabpost"> <i class="fa fa-table fa-fw"></i> Ads</a>
</li>

<li>
<a href="companies.php" id="tabpost"> <i class="fa fa-table fa-fw"></i> Companies</a>
</li>

<li>
<a href="category.php" id="tabcategory"><i class="fa fa-table fa-fw"></i> Category</a>
</li>
<li>
<a href="comp-category.php" id="tabcategory"><i class="fa fa-table fa-fw"></i>Company Category</a>
</li>
<li>
<a href="ad-post.php" id="tabcategory"><i class="fa fa-edit fa-fw"></i> Ad Post</a>
</li>
<li>
<a href="company-post.php" id="tabcategory"><i class="fa fa-edit fa-fw"></i> Company Post</a>
</li>




</ul>
</div>
<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>

<div id="page-wrapper">
<br>
<!-- /.row -->
<div class="row">
<div class="col-lg-6" id="divpost">
<div class="panel panel-default">
<div class="panel-heading">
<i class="fa fa-bar-chart-o fa-fw"></i> Upload Product


</div>
<!-- /.panel-heading -->
<div class="panel-body">
<form method="post" action="post-back.php" enctype="multipart/form-data">
<div class="form-group">
<label>Ad Title</label>
<input class="form-control" placeholder="Enter Your Ad Title" maxlength="70" name="adtitle" required>
</div>
<!-- <div class="form-group">
<label>Url Slug</label>
<input class="form-control" placeholder="Enter Url Slug" maxlength="70" name="urlslug" required>
</div> -->

<div class="form-group">
<label>Ad Description</label>
<textarea class="form-control" placeholder="Enter Your Short Description" rows="4" name="addesc"></textarea>
</div>

<div class="form-group">
<label>Listing Type</label>
<select class="form-control"  name="adtype">
  <option value="">Select</option>
  <option value="Free">Free</option>
  <option value="Premium">Premium</option>
  </select>
</div>

<div class="form-group">
<label>Select Category</label>
<select class="form-control"  name="categories" required>
    <option value="">Select</option>
  <?php  while($row = mysqli_fetch_array($data)){ ?>

    <option value="<?php echo $row['cat_name']; ?>"><?php echo $row['cat_name']; ?></option>
   
  <?php } ?>
  </select>
</div>


<div class="form-group">
<label>Contact Name</label>
<input class="form-control" placeholder="Enter Your Name" maxlength="200" name="cname" >
</div>
<div class="form-group">
<label>Contact Mobile Number</label>
<input type="text" class="form-control" placeholder="Enter Your Mobile Number" name="cmobile" maxlength="10" >
</div>
<div class="form-group">
<label>Contact Email Id</label>
<input type="email" class="form-control" placeholder="Enter Your Email ID" maxlength="200" name="cemail" >
</div>
<!-- <div class="form-group">
<label>Company Name</label>
<input class="form-control" placeholder="Enter Your Company Name" maxlength="200" name="companyname" required>
</div>
<div class="form-group">
<label>City</label>
<input class="form-control" placeholder="Enter Your City" maxlength="200" name="city" required>
</div> -->


<div class="form-group">
<label>Thumb Image</label>
<input class="form-control" type="file" name="FileToUpload" id="FileToUpload">
</div>


<div class="form-group">
<label>Main Image</label>

<input class="form-control" type="file" name="FileToUpload1" id="FileToUpload1">

</div> 

<div class="form-group">
<label>Seo Title</label>
<input class="form-control" placeholder="Enter url with -" name="seotitle">
</div>


<div class="form-group">
<label>Seo Description</label>
<textarea class="form-control" placeholder="Enter url with -" name="seodesc" rows="3"></textarea>
</div>


<div class="form-group">
<label>Seo Url</label>
<input class="form-control" placeholder="Enter url with -" name="seourl">
</div>


<div class="form-group">
<input class="btn-success" type="submit" class="btn btn-default" value="Upload Product">
</div>
</form>
</div>
<!-- /.panel-body -->
</div>

</div>

</div>
<!-- /.row -->

<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="vendor/raphael/raphael.min.js"></script>
<script src="vendor/morrisjs/morris.min.js"></script>
<script src="data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
<script>
$(document).ready(function(){
// $("#divpost").hide();
$("#tabpost").click(function(){
$("#divpost").show();
});
$("#show").click(function(){
$("p").show();
});
});

<?php } ?>
</script>
