<?php

if(isset($_POST["id"]) && !empty($_POST["id"])){


$postid = $_POST["id"];

//include database configuration file
include 'includes/config.php';

//count all rows except already displayed
$queryAll = mysqli_query($dbc,"SELECT COUNT(*) as num_rows FROM ad_table WHERE ad_id < '$postid' ORDER BY ad_id DESC");
$row = mysqli_fetch_assoc($queryAll);
$allRows = $row['num_rows'];

$showLimit = 12;

//get rows query
$query = mysqli_query($dbc, "SELECT * FROM ad_table WHERE ad_id < '$postid' ORDER BY ad_id DESC LIMIT ".$showLimit);

//number of rows
$rowCount = mysqli_num_rows($query);

if($rowCount > 0){ 
    while($row = mysqli_fetch_assoc($query)){ 
        $tutorial_id = $row["ad_id"]; ?>
       
                  <div class="product-item column">

                            <a href="javascript:void(0);">
                            <!-- PRODUCT PREVIEW ACTIONS -->
                            <div class="product-preview-actions">
                                <!-- PRODUCT PREVIEW IMAGE -->
                                <figure class="product-preview-image">
                                    <img src="user/upload/<?php 

                                    if($row['ad_image'] == ''){

                                        echo "not-found.png";

                                    }else{

                                     echo $row['ad_image'];

                                    }

                                      ?>" alt="product-image">
                                </figure>
                                
                            </div>
                            <!-- /PRODUCT PREVIEW ACTIONS -->

                            <!-- PRODUCT INFO -->
                            <div class="product-info">
                                <a href="ads/<?php echo $row['ad_id']; ?>"><p class="text-header">
                                        
                                    <?php


                                    if($row['ad_title'] == ''){

                                        echo "NA";

                                    }else{

                                     echo $row['ad_title'];

                                    }

                                      ?>


                                    </p></a>
                                    
                                
                                <p class="product-description"><?php 


                                $string1 =  $row['ad_desc'];

                                $string = strip_tags($string1);

                                if (strlen($string) > 70) {

                                    // truncate string
                                    $stringCut = substr($string, 0, 70);

                                  
                                   $string = substr($stringCut, 0, strrpos($stringCut, ' ')); 


                                }
                            
                                    if($string == ''){

                                        echo "NA";

                                    }else{

                                     echo $string;

                                    }

                                    


                                 ?></p>
                                
                            </div>
                            <!-- /PRODUCT INFO -->
                            <hr class="line-separator">

                            <!-- USER RATING -->
                            <div class="user-rating">
                                
                                <a href="#">
                                    <p class="text-header tiny">
                                        
                                    <?php


                                    if($row['contact_name'] == ''){

                                        echo "NA";

                                    }else{

                                     echo $row['contact_name'];

                                    }

                                      ?>




                                    </p>
                                </a>
                                
                            </div>
                          </a>  <!-- /USER RATING -->
                        </div>
<?php } ?>
<?php if($allRows > $showLimit){ ?>
    <div class="show_more_main" id="show_more_main<?php echo $tutorial_id; ?>">
        <span id="<?php echo $tutorial_id; ?>" class="show_more" title="Load more posts">Show more</span>
        <span class="loding" style="display: none;"><span class="loding_txt">Loading….</span></span>
    </div>
<?php } ?>  


<?php 
    } 
}
?>