<?php
session_start();

if(!isset($_SESSION["user_email"])&&($_SESSION["user_email"]==''))
{
  header("Location: login.php");
  
}
else
{

   $userid = $_SESSION['user_id'];

  include 'config.php';

  $query = "SELECT * from companies where user_id='$userid' ";

  $data = mysqli_query($dbc,$query)or die(mysqli_error($dbc));


  ?>


<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Showing all Ads</title>

<!-- Bootstrap Core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="vendor/morrisjs/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>

<body>

<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="/index.html">Home</a>

</div>
<!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right">

<!-- /.dropdown -->


<!-- /.dropdown -->
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">
<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
</a>
<ul class="dropdown-menu dropdown-user">
<li><a href="/index.html"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['user_name']; ?></a>
</li>

<li class="divider"></li>
<li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
</li>
</ul>
<!-- /.dropdown-user -->
</li>
<!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->

<div class="navbar-default sidebar" role="navigation">
<div class="sidebar-nav navbar-collapse">
<ul class="nav" id="side-menu">

<li>
<a href="index.php" id="tabdashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
</li>

<li>
<a href="showads.php" id="tabuser"><i class="fa fa-table fa-fw"></i>Show Ads</a>
</li>

<li>
<a href="ad-post.php" id="tabuser"><i class="fa fa-table fa-fw"></i>Create Ad</a>
</li>

<li>
<a href="company-post.php" id="tabuser"><i class="fa fa-table fa-fw"></i>Post Company</a>
</li>

<li>
<a href="showcompany.php" id="tabuser"><i class="fa fa-table fa-fw"></i>Show Companies</a>
</li>




</ul>
</div>
<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>

<div id="page-wrapper">
  <h2>Showing all Companies</h2>
      
  <table class="table table-bordered">
 
  
    <thead>
      <tr>
        <th>Ad ID</th>
        <th>AD Title</th>
        <th>Contact Name</th>
        <th>Mobile No</th>
         <th>Approve Status</th>
          <th>Edit</th>
          <th>Delete</th>
      </tr>
    </thead>
    <tbody>
      <tr>

        <?php  

        $i = 1;

        while($row = mysqli_fetch_array($data, MYSQLI_ASSOC)){ 

        $postid = $row['comp_id'];


        ?>

        <td><?php echo $i; ?></td>
        <td><?php echo $row['comp_title']; ?></td>
        <td><?php echo $row['contact_person']; ?></td>
        <td><?php echo $row['contact_mobile']; ?></td>
        <td><?php echo $row['approve_status']; ?></td>
        <td><a href="edit-company.php?id=<?php echo $postid; ?>">Edit</a></td>
        <td><a href="delete-company.php?id=<?php echo $postid; ?>">Delete</a></td>

        <?php   $i++; ?>
      </tr>
    </tbody>
 <?php } ?>
  
  </table>

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="vendor/raphael/raphael.min.js"></script>
<script src="vendor/morrisjs/morris.min.js"></script>
<script src="data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

<?php } ?>
</body>

</html>
<script>
$(document).ready(function(){
// $("#divpost").hide();
$("#tabpost").click(function(){
$("#divpost").show();
});
$("#show").click(function(){
$("p").show();
});
});


</script>
