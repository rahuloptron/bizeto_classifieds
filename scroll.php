
<?php
//Include DB configuration file
include 'includes/config.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="scroll.css">
</head>
<body>
  
<div class="tutorial_list">
    <?php
    //include database configuration file
    
    //get rows query
    $query = mysqli_query($dbc, "SELECT * FROM ad_table ORDER BY ad_id DESC LIMIT 8");
    
    //number of rows
    $rowCount = mysqli_num_rows($query);
    
    if($rowCount > 0){ 
        while($row = mysqli_fetch_assoc($query)){ 
            $tutorial_id = $row['ad_id'];
    ?>
        <div class="list_item"><a href="javascript:void(0);"><h2><?php echo $row["ad_title"]; ?></h2></a></div>
    <?php } ?>
    <div class="show_more_main" id="show_more_main<?php echo $tutorial_id; ?>">
        <span id="<?php echo $tutorial_id; ?>" class="show_more" title="Load more posts">Show more</span>
        <span class="loding" style="display: none;"><span class="loding_txt">Loading....</span></span>
    </div>
    <?php } ?>
</div>

<script src="../js/vendor/jquery-3.1.0.min.js"></script>
<script src="/scroll.js"></script>

</body>
</html>