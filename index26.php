<?php

session_start();

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

include 'includes/config.php';

$query = "SELECT * from ad_table where approve_status = '1' ORDER BY ad_id DESC";

$data = mysqli_query($dbc,$query)or die(mysqli_error($dbc));

$query1 = "SELECT * from companies where approve_status = '1' ORDER BY comp_id DESC";

$data1 = mysqli_query($dbc,$query1)or die(mysqli_error($dbc));

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="stylesheet" href="css/vendor/simple-line-icons.css">
	<link rel="stylesheet" href="css/vendor/tooltipster.css">
	<link rel="stylesheet" href="css/vendor/owl.carousel.css">
	<link rel="stylesheet" href="css/style.css">
	<!-- favicon -->
	<link rel="icon" href="favicon.ico">
	<title> Home</title>
</head>
<body>

	<!-- HEADER -->
	<div class="header-wrap">

<?php include 'includes/header.php' ?>

	</div>
	<!-- /HEADER -->




	<?php include 'includes/menu-dark.php' ?>

	<!-- BANNER -->
	<div class="banner-wrap">
		<section class="banner">
			<h5>Welcome to</h5>
			<h1>The Biggest <span>Marketplace</span></h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
			

		</section>
	</div>
	<!-- /BANNER -->

	<!-- SERVICES -->
	<div id="services-wrap">
		<section id="services">
			<!-- SERVICE LIST -->
			<div class="service-list column4-wrap">
				<!-- SERVICE ITEM -->
				<div class="service-item column">
					<div class="circle medium gradient"></div>
					<div class="circle white-cover"></div>
					<div class="circle dark">
						<span class="icon-present"></span>
					</div>
					<h3>Buy &amp; Sell Easily</h3>
					<p>Buy & Sell products online without credit card</p>
				</div>
				<!-- /SERVICE ITEM -->

				<!-- SERVICE ITEM -->
				<div class="service-item column">
					<div class="circle medium gradient"></div>
					<div class="circle white-cover"></div>
					<div class="circle dark">
						<span class="icon-tag"></span>
					</div>
					<h3>Discounts & Offers</h3>
					<p>Get exciting offers & exclusive dsicount from genuine sellers</p>
				</div>
				<!-- /SERVICE ITEM -->

				<!-- SERVICE ITEM -->
				<div class="service-item column">
					<div class="circle medium gradient"></div>
					<div class="circle white-cover"></div>
					<div class="circle dark">
						<span class="icon-like"></span>
					</div>
					<h3>Product Reviews</h3>
					<p>Create reviews & rate products online</p>
				</div>
				<!-- /SERVICE ITEM -->

				<!-- SERVICE ITEM -->
				<div class="service-item column">
					<div class="circle medium gradient"></div>
					<div class="circle white-cover"></div>
					<div class="circle dark">
						<span class="icon-diamond"></span>
					</div>
					<h3>Easily Connect</h3>
					<p>Easliy connect with seller suing whatsaap or email</p>
				</div>
				<!-- /SERVICE ITEM -->
			</div>
			<!-- /SERVICE LIST -->
			<div class="clearfix"></div>
		</section>
	</div>
	<!-- /SERVICES -->

	<!-- PROMO -->
	<div class="promo-banner dark left">
		<span class="icon-wallet"></span>
		<h5>Sell more Product</h5>
		<h1>Start <span>Selling</span></h1>
		<a href="#" class="button medium primary">Open Your Shop!</a>
	</div>
	<!-- /PROMO -->

	<!-- PROMO -->
	<div class="promo-banner secondary right">
		<span class="icon-present"></span>
		<h5>Find anything you want</h5>
		<h1>Start Buying</h1>
		<a href="#" class="button medium dark">Register Now!</a>
	</div>
	<!-- /PROMO -->

	<div class="clearfix"></div>

	<!-- PRODUCT SIDESHOW -->
	<div id="product-sideshow-wrap">
		<div id="product-sideshow">
			<!-- PRODUCT SHOWCASE -->
			<div class="product-showcase">
				<!-- HEADLINE -->
				<div class="headline primary">
					<h4>Products Ads</h4>
					<!-- SLIDE CONTROLS -->
					<div class="slide-control-wrap">
						<div class="slide-control left">
							<!-- SVG ARROW -->
							<svg class="svg-arrow">
								<use xlink:href="#svg-arrow"></use>
							</svg>
							<!-- /SVG ARROW -->
						</div>

						<div class="slide-control right">
							<!-- SVG ARROW -->
							<svg class="svg-arrow">
								<use xlink:href="#svg-arrow"></use>
							</svg>
							<!-- /SVG ARROW -->
						</div>
					</div>
					<!-- /SLIDE CONTROLS -->
				</div>
				<!-- /HEADLINE -->

				<!-- PRODUCT LIST -->
				<div id="pl-1" class="product-list grid column4-wrap owl-carousel">
				
					<?php while($row = mysqli_fetch_array($data)){ ?>
					<div class="product-item column">


							<!-- PRODUCT PREVIEW ACTIONS -->
							<div class="product-preview-actions">
								<!-- PRODUCT PREVIEW IMAGE -->
								<figure class="product-preview-image">
									<img src="user/upload/

									<?php 

									if($row['ad_image'] == ''){

										echo "not-found.png";

									}else{

									 echo $row['ad_image'];

									}

									  ?>" alt="product-image">
								</figure>
								
							</div>
							<!-- /PRODUCT PREVIEW ACTIONS -->

							<!-- PRODUCT INFO -->
							<div class="product-info">
								<a href="ads/<?php echo $row['ad_id']; ?>"><p class="text-header">
										
									<?php


									if($row['ad_title'] == ''){

										echo "NA";

									}else{

									 echo $row['ad_title'];

									}

									  ?>


									</p></a>
									
								
								<p class="product-description"><?php 


								$string1 = 	$row['ad_desc'];

								$string = strip_tags($string1);

								if (strlen($string) > 70) {

								    // truncate string
								    $stringCut = substr($string, 0, 70);

								  
								   $string = substr($stringCut, 0, strrpos($stringCut, ' ')); 


								}
							
									if($string == ''){

										echo "NA";

									}else{

									 echo $string;

									}

									


								 ?></p>
								
							</div>
							<!-- /PRODUCT INFO -->
							<hr class="line-separator">

							<!-- USER RATING -->
							<div class="user-rating">
								
								<a href="#">
									<p class="text-header tiny">
										
									<?php


									if($row['contact_name'] == ''){

										echo "NA";

									}else{

									 echo $row['contact_name'];

									}

									  ?>




									</p>
								</a>
								
							</div>
							<!-- /USER RATING -->
						</div>
					<?php } ?>
				</div>
				<!-- /PRODUCT LIST -->

			</div>
			<!-- /PRODUCT SHOWCASE -->

			<!-- PRODUCT SHOWCASE -->
			<div class="product-showcase">
				<!-- HEADLINE -->
				<div class="headline secondary">
					<h4>Companies</h4>
					<!-- SLIDE CONTROLS -->
					<div class="slide-control-wrap">
						<div class="slide-control left">
							<!-- SVG ARROW -->
							<svg class="svg-arrow">
								<use xlink:href="#svg-arrow"></use>
							</svg>
							<!-- /SVG ARROW -->
						</div>

						<div class="slide-control right">
							<!-- SVG ARROW -->
							<svg class="svg-arrow">
								<use xlink:href="#svg-arrow"></use>
							</svg>
							<!-- /SVG ARROW -->
						</div>
					</div>
					<!-- /SLIDE CONTROLS -->
				</div>
				<!-- /HEADLINE -->

				<!-- PRODUCT LIST -->
				<div id="pl-4" class="product-list grid column4-wrap owl-carousel">
					
					<?php while($row1 = mysqli_fetch_array($data1)) { ?>
					<div class="product-item column">


							<!-- PRODUCT PREVIEW ACTIONS -->
							<div class="product-preview-actions">
								<!-- PRODUCT PREVIEW IMAGE -->
								<figure class="product-preview-image">
									<img src="user/upload/logo/

									<?php 

									if($row1['comp_logo'] == ''){

										echo "not-found.png";

									}else{

									 echo $row1['comp_logo'];

									}

									  ?>" alt="product-image">
								</figure>
								
							</div>
							<!-- /PRODUCT PREVIEW ACTIONS -->

							<!-- PRODUCT INFO -->
							<div class="product-info">
								<a href="<?php echo $row1['url_slug']; ?>">
									<p class="text-header">
										
									<?php


									if($row1['comp_title'] == ''){

										echo "NA";

									}else{

									 echo $row1['comp_title'];

									}

									  ?>

									</p>
								</a>
								<p class="product-description"><?php 


								$string1 = 	$row1['comp_desc'];

								$string = strip_tags($string1);

								if (strlen($string) > 70) {

								    // truncate string
								    $stringCut = substr($string, 0, 70);

								  
								   $string = substr($stringCut, 0, strrpos($stringCut, ' ')); 


								}
							
									if($string == ''){

										echo "NA";

									}else{

									 echo $string;

									}

									


								 ?></p>
								
							</div>
							<!-- /PRODUCT INFO -->
							<hr class="line-separator">

							<!-- USER RATING -->
							<div class="user-rating">
								
								<a href="#">
									<p class="text-header tiny">
										
									<?php


									if($row1['contact_person'] == ''){

										echo "NA";

									}else{

									 echo $row1['contact_person'];

									}

									  ?>




									</p>
								</a>
								
							</div>
							<!-- /USER RATING -->
						</div>
					<?php } ?>
				</div>
				<!-- /PRODUCT LIST -->
			</div>
			<!-- /PRODUCT SHOWCASE -->

			<!-- PRODUCT SHOWCASE -->
			
			<!-- PRODUCT SHOWCASE -->
		</div>
	</div>
	<!-- /PRODUCTS SIDESHOW -->

	<!-- SUBSCRIBE BANNER -->
	<div id="subscribe-banner-wrap">
		<div id="subscribe-banner">
			<!-- SUBSCRIBE CONTENT -->
			<div class="subscribe-content">
				<!-- SUBSCRIBE HEADER -->
				<div class="subscribe-header">
					<figure>
						<img src="images/mobile.png" alt="subscribe-icon">
					</figure>
					<p class="subscribe-title">Get Amazing Offers</p>
					<p>Enter your mobile no to get offers</p>
				</div>
				<!-- /SUBSCRIBE HEADER -->

				<!-- SUBSCRIBE FORM -->
				<form class="subscribe-form">
					<input type="text" name="subscribe_email" id="subscribe_email" placeholder="Enter your mobile no here...">
					<button class="button medium dark">Get Discount</button>
				</form>
				<!-- /SUBSCRIBE FORM -->
			</div>
			<!-- /SUBSCRIBE CONTENT -->
		</div>
	</div>
	<!-- /SUBSCRIBE BANNER -->

	<!-- FOOTER -->
		<?php include 'includes/footer.php' ?>
	<!-- /FOOTER -->

	<div class="shadow-film closed"></div>

<!-- SVG ARROW -->
<svg style="display: none;">	
	<symbol id="svg-arrow" viewBox="0 0 3.923 6.64014" preserveAspectRatio="xMinYMin meet">
		<path d="M3.711,2.92L0.994,0.202c-0.215-0.213-0.562-0.213-0.776,0c-0.215,0.215-0.215,0.562,0,0.777l2.329,2.329
			L0.217,5.638c-0.215,0.215-0.214,0.562,0,0.776c0.214,0.214,0.562,0.215,0.776,0l2.717-2.718C3.925,3.482,3.925,3.135,3.711,2.92z"/>
	</symbol>
</svg>
<!-- /SVG ARROW -->

<!-- SVG STAR -->
<svg style="display: none;">
	<symbol id="svg-star" viewBox="0 0 10 10" preserveAspectRatio="xMinYMin meet">	
		<polygon points="4.994,0.249 6.538,3.376 9.99,3.878 7.492,6.313 8.082,9.751 4.994,8.129 1.907,9.751 
	2.495,6.313 -0.002,3.878 3.45,3.376 "/>
	</symbol>
</svg>
<!-- /SVG STAR -->

<!-- SVG PLUS -->
<svg style="display: none;">
	<symbol id="svg-plus" viewBox="0 0 13 13" preserveAspectRatio="xMinYMin meet">
		<rect x="5" width="3" height="13"/>
		<rect y="5" width="13" height="3"/>
	</symbol>
</svg>
<!-- /SVG PLUS -->

<!-- jQuery -->
<script src="js/vendor/jquery-3.1.0.min.js"></script>
<!-- Tooltipster -->
<script src="js/vendor/jquery.tooltipster.min.js"></script>
<!-- Owl Carousel -->
<script src="js/vendor/owl.carousel.min.js"></script>
<!-- Tweet -->
<script src="js/vendor/twitter/jquery.tweet.min.js"></script>
<!-- xmAlerts -->
<script src="js/vendor/jquery.xmalert.min.js"></script>
<!-- Side Menu -->
<script src="js/side-menu.js"></script>
<!-- Home -->
<script src="js/home.js"></script>
<!-- Tooltip -->
<script src="js/tooltip.js"></script>
<!-- User Quickview Dropdown -->
<script src="js/user-board.js"></script>

<!-- Footer -->
<script src="js/footer.js"></script>
</body>
</html>